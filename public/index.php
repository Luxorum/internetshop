<?php
session_start();
use internetshop\App;

require dirname(__DIR__) . '/config/init.php';
require_once LIBS . '/functions.php';
require_once CONF . '/routes.php';
new App();

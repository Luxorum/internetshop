<?use internetshop\ErrorHandler?>
<!DOCTYPE HTML>
<html>
<head>
    <title>404</title>
</head>


<h1>Виникла помилка</h1>
<p><b>Код помилки:</b> <?= $errnum?></p>
<p><b>Текст ошибки:</b> <?= $errstr ?></p>
<p><b>Файл, в котором произошла ошибка:</b> <?= $errfile ?></p>
<p><b>Строка, в которой произошла ошибка:</b> <?= $errline ?></p>
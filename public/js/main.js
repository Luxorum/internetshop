(function ($) {
 "use strict";
 
/*----------------------------
 Data-Toggle Tooltip
------------------------------ */	

$('[data-toggle="tooltip"]').tooltip();
 
 /*----------------------------
 wow js active
------------------------------ */
 new WOW().init();
 
/*----------------------------
 jQuery MeanMenu
------------------------------ */
	jQuery('nav#mobile-menu').meanmenu();

	jQuery('nav#mobile-menu1').meanmenu();

/*----------------------------
 jQuery MeanMenu
------------------------------ */
	$('.dropdown-toggle').dropdown();

//---------------------------------------------
//Nivo slider
//---------------------------------------------
	$('#ensign-nivoslider').nivoSlider({
		autoplay: true,
		slices: 15,
		animSpeed: 500,
		pauseTime: 5000,
		directionNav: true,
		pauseOnHover: false,
	});
	 
 /*----------------------------
 Active-Hot-Deals
------------------------------ */  
  $(".active-hot-deals").owlCarousel({
      autoPlay: false, 
	  slideSpeed:2000,
	  pagination:true,
	  navigation:false,	  
      items : 1,
	  navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
	  itemsDesktop : [1169,1],
	  itemsTablet: [991,1],
	  itemsTabletSmall: [767,2],
	  itemsMobile : [479,1],
  });

/*----------------------------
 Active-Bestseller
------------------------------ */  
  $(".active-bestseller").owlCarousel({
      autoPlay: false, 
	  slideSpeed:2000,
	  pagination:false,
	  navigation:true,	  
      items : 1,
	  navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
	  itemsDesktop : [1169,1],
	  itemsTablet: [991,1],
	  itemsTabletSmall: [767,1],
	  itemsMobile : [479,1],
  });

/*----------------------------
 Active-Sidebar-Banner
------------------------------ */  
  $(".active-sidebar-banner").owlCarousel({
      autoPlay: true, 
	  slideSpeed:2000,
	  pagination:false,
	  navigation:false,	  
      items : 1,
	  transitionStyle : "fade",
	  itemsDesktop : [1169,1],
	  itemsTablet: [991,1],
	  itemsTabletSmall: [767,1],
	  itemsMobile : [479,1],
  });

/*----------------------------
 Active-Recent-Posts
------------------------------ */  
  $(".active-recent-posts").owlCarousel({
      autoPlay: false, 
	  slideSpeed:2000,
	  pagination:true,
	  navigation:false,	  
      items : 1,
	  itemsDesktop : [1169,1],
	  itemsTablet: [991,1],
	  itemsTabletSmall: [767,1],
	  itemsMobile : [479,1],
  });

 /*----------------------------
 Active-Product-Carosel
------------------------------ */   
  $(".active-product-carosel").owlCarousel({
      autoPlay: false, 
	  slideSpeed:2000,
	  pagination:false,
	  navigation:true,	  
      items : 4,
	  navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
	  itemsDesktop : [1169,3],
	  itemsTablet: [991,2],
	  itemsTabletSmall: [767,2],
	  itemsMobile : [479,1],	  
  });
  
 /*----------------------------
 Active-Small-Product
------------------------------ */   
  $(".active-small-product").owlCarousel({
      autoPlay: false, 
	  slideSpeed:2000,
	  pagination:false,
	  navigation:true,	  
      items : 3,
	  navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
	  itemsDesktop : [1169,2],
	  itemsTablet: [991,2],
	  itemsTabletSmall: [767,1],
	  itemsMobile : [479,1],	
  });

 /*----------------------------
 Active-Brand-Logo
------------------------------ */   
  $(".active-brand-logo").owlCarousel({
      autoPlay: false, 
	  slideSpeed:2000,
	  pagination:false,
	  navigation:true,	  
      items : 6,
	  navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
	  itemsDesktop : [1169,5],
	  itemsTablet: [991,4],
	  itemsTabletSmall: [767,2],
	  itemsMobile : [479,1],
  });

 /*----------------------------
 Active-Hot-Deals-Style-2
------------------------------ */  
  $(".active-hot-deals-style-2").owlCarousel({
      autoPlay: false, 
	  slideSpeed:2000,
	  pagination:false,
	  navigation:false,	  
      items : 5,
	  itemsDesktop : [1169,4],
	  itemsTablet: [991,3],
	  itemsTabletSmall: [767,2],
	  itemsMobile : [479,1],
  });

 /*----------------------------
 Active-Product-Carosel-style-2
------------------------------ */   
  $(".active-product-carosel-style-2").owlCarousel({
      autoPlay: false, 
	  slideSpeed:2000,
	  pagination:false,
	  navigation:true,	  
      items : 5,
	  navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
	  itemsDesktop : [1169,4],
	  itemsTablet: [991,3],
	  itemsTabletSmall: [767,2],
	  itemsMobile : [479,1],
  });

 /*----------------------------
 	Active-Recent-Posts-style-2
------------------------------  */  
  $(".active-recent-posts-style-2").owlCarousel({
      autoPlay: false, 
	  slideSpeed:2000,
	  pagination:false,
	  navigation:true,	  
      items : 4,
	  navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
	  itemsDesktop : [1169,4],
	  itemsTablet: [991,3],
	  itemsTabletSmall: [767,2],
	  itemsMobile : [479,1],
  });


/*--------------------------
	Category Menu
---------------------------- */	
	 $('.rx-parent').on('click', function(){
		$('.rx-child').slideToggle();
		$(this).toggleClass('rx-change');
		
	});

	$(".embed-responsive iframe").addClass("embed-responsive-item");
	$(".carousel-inner .item:first-child").addClass("active");
	
/*--------------------------
	category left menu
---------------------------- */	
	 $('.category-heading').on('click', function(){
	 $('.category-menu-list').slideToggle(300);
	});	  


/*---------------------
 countdown
--------------------- */
	$('[data-countdown]').each(function() {
	  var $this = $(this), finalDate = $(this).data('countdown');
	  $this.countdown(finalDate, function(event) {
		$this.html(event.strftime('<span class="cdown days"><span class="time-count">%-D</span> <p>Days</p></span> <span class="cdown hour"><span class="time-count">%-H</span> <p>Hour</p></span> <span class="cdown minutes"><span class="time-count">%M</span> <p>Min</p></span> <span class="cdown second"> <span><span class="time-count">%S</span> <p>Sec</p></span>'));
	  });
	});	


/*---------------------
 price slider
--------------------- */  
	$(function() {
	  $( "#slider-range" ).slider({
	   range: true,
	   min: 40,
	   max: 600,
	   values: [ 60, 570 ],
	   slide: function( event, ui ) {
		$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
	   }
	  });
	  $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
	   " - $" + $( "#slider-range" ).slider( "values", 1 ) );
	});	


/*--------------------------
 scrollUp
---------------------------- */	
	$.scrollUp({
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    }); 	





 
})(jQuery);





/*Cart*/
$('body').on('click','.add-to-cart',function (e) {
	e.preventDefault();
	let id = $(this).data('id'),
		qty = $('.quantity').val() ? $('.quantity').val() : 1,
		color = $('#color').find(":selected").index() + 1;
		size = $('#size').find(":selected").index() + 1;
	$.ajax({
		url: '/cart/add',
		data: {id: id,qty:qty,color: color,size: size},
		type: 'GET',
		success: function(){
			window.location.reload();
		},
		error: function () {
			alert('Помилка, спробуйте пізніше');
		}
	});
});
// function showCart(cart) {
// 	console.log(cart);
// }
$('#color').on('change', function(){
	var colorId = $(this).val(),
		price = $(this).find('option').filter(':selected').data('price'),
		price2 = $('#size').find('option').filter(':selected').data('price'),
		basePrice = $('#base-price').data('price');
	if(price>=0){
		$('.current-price ').text("$"+(+basePrice+(+price)+(+price2)).toFixed(2));
	}
});
$('#size').on('change', function(){
	var sizeId = $(this).val(),

		price = $(this).find('option').filter(':selected').data('price'),
		price2 = $('#color').find('option').filter(':selected').data('price'),
		basePrice = $('#base-price').data('price');
	if(price>=0){
		$('.current-price ').text("$"+(+basePrice+(+price)+(+price2)).toFixed(2));
	}
});

$('.delete-cart').on('click', function(){
	let id = $(this).data('id');
	$.ajax({
		url: '/cart/delete',
		data: {id: id},
		type: 'GET',
		success: function(){
			location.reload();
		},
		error: function(){
			location.reload();
		}
	});
});

$('#clear-cart').on('click', function(){
	let id = $(this).data('id');
	$.ajax({
		url: '/cart/clear',
		data: {id: id},
		type: 'GET',
		success: function(){
			location.reload();
		},
		error: function(){
			alert('Error!');
		}
	});
});
/*Cart*/

//Search get ajax
/* Search */
var products = new Bloodhound({
	datumTokenizer: Bloodhound.tokenizers.whitespace,
	queryTokenizer: Bloodhound.tokenizers.whitespace,
	remote: {
		wildcard: '%QUERY',
		url: path + '/search/typeahead?query=%QUERY'
	}
});

products.initialize();

$("#typeahead").typeahead({
	// hint: false,
	highlight: true
},{
	name: 'Product',
	display: 'model',
	limit: 9,
	source: products
});


//End

// Form validate

function validate() {
	$('#payment-address').attr('style','height:384px;');
	$('#payment-address').attr('aria-expanded','true');
	$('#payment-address').attr('class','collapse in');
	$('#form1').attr('class','accordion-trigger');
	$('#form1').attr('aria-expanded','true');
}
//End form validate

$('body').on('click', '#filter', function(){
	var checked = $('.accordion_one input:checked'),
		data = '';
	checked.each(function () {
		data += this.value + ',';
	});
	if(data){
		$.ajax({
			url: location.href,
			data: {filter: data},
			type: 'GET',
			beforeSend: function(){
					$('.listview').hide();

			},
			success: function(res){
					$('.listview').fadeIn(res);
					var url = location.search.replace(/filter(.+?)(&|$)/g, ''); //$2
					var newURL = location.pathname + url + (location.search ? "&" : "?") + "filter=" + data;
					newURL = newURL.replace('&&', '&');
					newURL = newURL.replace('?&', '?');
					history.pushState({}, '', newURL);
					location.reload();

				},

			error: function () {
				alert('Ошибка!');
			}
		});
	}else{
		window.location = location.pathname;
	}
});
$('#sort').on('change',function () {
	let data = this.selectedIndex;
	$.ajax({
		url: location.href,
		data: {sort:data},
		type: 'GET',
		success: function () {
			let url = location.search.replace(/,$/,'');
			url = url.replace(/&sort(.+?)(&|$)/,'');
			let newURL = location.pathname + url + (location.search ? "&" : "?") + "sort=" + data;
			history.pushState({}, '', newURL);
			location.reload();
		},
		error: function () {
			console.log(location.search);
		}
	});
});
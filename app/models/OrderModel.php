<?php


namespace app\models;


class OrderModel extends AppModel
{
    public function getOrders($start,$perpage){
        return \R::getAll("SELECT * FROM `Order` ORDER BY id LIMIT $start,$perpage");
    }
    public function getOrdersCount(){
        return \R::count('order');
    }
    public function loadOrder($order_id){
        return \R::load('order', $order_id);
    }
    public function deleteOrder($order){
        \R::trash($order);
    }
    public function getJoinOrder($order_id){
        return \R::getRow("SELECT `Order`.*, ROUND(SUM(`Order_Product`.`price`), 2) AS `sum` FROM `Order`
  JOIN `Order_Product` ON `order`.`id` = `Order_Product`.`order_id`
  WHERE `Order`.`id` = ?
   LIMIT 1", [$order_id]);

    }
}
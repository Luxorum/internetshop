<?php


namespace app\models;


class FilterModel extends AppModel
{
    function getAttribute()
    {
        return \R::getAssoc("SELECT attribute_value.*, attribute_group.title FROM attribute_value JOIN attribute_group ON attribute_group.id = attribute_value.attribute_group_id");
    }
    function getGroupAttribute(){
        return \R::findAll('attribute_group');
    }
    public function update($table, $id){
        $bean = \R::load($table, $id);
        foreach($this->attributes as $name => $value){
            $bean->$name = $value;
        }
        return \R::store($bean);
    }
    public function loadGroup($id){
        return \R::load('attribute_group', $id);
    }
    public function loadAttribute($id){
        return \R::load('attribute_value', $id);
    }
    public function findAllAttributes(){
       return \R::findAll('attribute_group');
    }
    public function getGroupIdByName($title){
        return \R::getAll("SELECT id FROM Attribute_Group WHERE title = '{$title}'");
    }
    public function addAttribute($title,$id){
        \R::exec("INSERT INTO Attribute_Value (`value`,`attribute_group_id`) VALUES ('{$title}',{$id})");
    }
    public function addGroup($title){
        \R::exec("INSERT INTO Attribute_Group (title) VALUES ('{$title}')");
    }
    public function getCountGroups($id){
       return \R::count('attribute_value', 'attr_group_id = ?', [$id]);
    }
    public function deleteGroup($id){
        \R::exec('DELETE FROM attribute_group WHERE id = ?', [$id]);
    }
    public function deleteAttribute($id){
        \R::exec("DELETE FROM attribute_product WHERE attr_id = ?", [$id]);
        \R::exec("DELETE FROM attribute_value WHERE id = ?", [$id]);
    }
}
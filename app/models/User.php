<?php


namespace app\models;


class User extends AppModel
{
    public static function checkAdminAuth(){
        return isset($_SESSION['admin']);
    }
    public function getAdminData(){
        return \R::getAll('SELECT * FROM admin');
    }
    public static function logOut(){
        unset($_SESSION['admin']);
        unset($_SESSION['admin_name']);
        unset($_SESSION['error']);
    }

}
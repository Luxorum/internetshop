<?php


namespace app\models;


class Cart extends AppModel
{

    public function addToCart($product, $qty = 1, $color = null, $size = null)
    {
        $_SESSION['color'] = $color;
        $_SESSION['size'] = $size;
        if ($_SESSION['color'] && $_SESSION['size']) {
            $ID = "{$product->id}-{$color->id}-{$size->id}";
            $title = "{$product->model} ({$color->name})";
            debug($size->price);
            $price = $product->price + $color->price + $size->price;
        } else {
            $ID = $product->id;
            $title = $product->model;
            $price = $product->price;
        }
        if (isset($_SESSION['cart'][$ID])) {
            $_SESSION['cart'][$ID]['qty'] += $qty;
        } else {
            $_SESSION['cart'][$ID] = [
                'id' => $product->id,
                'qty' => $qty,
                'title' => $title,
                'alias' => $product->alias,
                'price' => $price,
                'img' => $product->img
            ];
        }
        $_SESSION['cart.qty'] = isset($_SESSION['cart.qty']) ? $_SESSION['cart.qty'] + $qty : $qty;
        $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? $_SESSION['cart.sum'] + $qty * $price : $qty * $price;
    }
    public function deleteItem($id){
        $qtyMinus = $_SESSION['cart'][$id]['qty'];
        $sumMinus = $_SESSION['cart'][$id]['qty'] * $_SESSION['cart'][$id]['price'];
        $_SESSION['cart.qty'] -= $qtyMinus;
        $_SESSION['cart.sum'] -= $sumMinus;
        unset($_SESSION['cart'][$id]);
    }
    public function clearCart(){
        unset($_SESSION['cart']);
        unset($_SESSION['cart.sum']);
        unset($_SESSION['cart.qty']);
    }
}
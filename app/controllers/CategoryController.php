<?php


namespace app\controllers;


use app\widgets\filter\Filter;
use internetshop\App;
use internetshop\libs\Pagination;

class CategoryController extends AppController
{
    public function viewAction(){
        $alias = $this->route['alias'];
        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $perpage = App::$app->getProperty('pagination');
        $category = $alias != 'all' ? \R::findOne('Category', 'alias = ?', [$alias]) : null;

        $sql_part = '';
        $sql_sort = '';
        if(!empty($_GET['filter'])){
            $filter = Filter::getFilter();
            if($filter){
                $cnt = Filter::getCountGroups($filter);
                if($alias !='all') {
                    $sql_part = "AND id IN (SELECT prod_id FROM Attribute_Product WHERE attr_id IN ($filter) GROUP BY prod_id HAVING COUNT(prod_id) = $cnt)";
                }
                else{
                    $sql_part = "WHERE id IN (SELECT prod_id FROM Attribute_Product WHERE attr_id IN ($filter) GROUP BY prod_id HAVING COUNT(prod_id) = $cnt)";
                }
            }
        }
        $_GET['sort'] = isset($_GET['sort']) ? $_GET['sort'] : 0;
        if(!empty($_GET['sort'])){
            $sort = $_GET['sort'];
            $_SESSION['sort'] = $sort;

            if($sort==1){
                $sql_sort = "ORDER BY model DESC";
            }
            else if($sort==2){
                $sql_sort = "ORDER BY model";
            }
            else if($sort==3){
                $sql_sort = "ORDER BY price DESC";
            }
            else if($sort==4){
                $sql_sort = "ORDER BY price DESC";
            }



        }
        else{
            if($_GET['sort']==0){
                $_SESSION['sort'] = 0;
            }
        }
        if($alias != 'all') {
            if (!$category) {
                    throw new \Exception('Такої категорії немає', 404);
                }

            $this->setMeta($category->name);
            $total = \R::count('product', "categoryId = ? $sql_part", [$category->id]);
        }
        else{
            $total = \R::count('product',"$sql_part");
            $this->setMeta('Всі товари');

        }



        $pagination = new Pagination($page,$perpage,$total);
        $start = $pagination->getStart();
        $products = $alias != 'all'
            ? \R::getAll("SELECT * FROM Product WHERE categoryId = {$category->id} $sql_part $sql_sort LIMIT $start, $perpage")
            : \R::getAll("SELECT * FROM Product $sql_part $sql_sort LIMIT $start, $perpage");
        $brands = $alias != 'all'
            ? \R::getAll("SELECT * FROM Brand WHERE categoryId = {$category->id}")
            : \R::getAll("SELECT * FROM Brand ");

        if($this->isAjax()){
            $this->loadView('filter', compact('products', 'total', 'pagination'));
        }





        $this->set(compact('products','category','pagination','total','start','perpage','brands'));
        $this->layout = 'product-list';
    }
}
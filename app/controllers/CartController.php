<?php


namespace app\controllers;


use app\models\Cart;

class CartController extends AppController
{


    public function addAction(){
        $id = !empty($_GET['id']) ? (int)$_GET['id'] : null;
        $qty = !empty($_GET['qty']) ? (int)$_GET['qty'] : null;
        $color_id = !empty($_GET['color']) ? (int)$_GET['color'] : null;
        $size_id = !empty($_GET['size']) ? (int)$_GET['size'] : null;
        $color = null;
        $size = null;
        if($id){
            $product = \R::findOne('Product','id = ?',[$id]);
            if(!$product){
                return false;
            }
            if($color_id){
                $color = \R::findOne('Colors', 'id = ?', [$color_id]);
                debug($color);
            }
            if($size_id){
                $size = \R::findOne('Sizes', 'id = ?', [$size_id]);
            }
        }
        $cart = new Cart();
        $cart->addToCart($product,$qty,$color,$size);
        if($this->isAjax()){
            $this->loadView('cart_modal');
        }
        else{
            redirect();
        }

    }
    public function deleteAction(){
        $id = !empty($_GET['id']) ? $_GET['id'] : null;
        if(isset($_SESSION['cart'][$id])){
            $cart = new Cart();
            $cart->deleteItem($id);
        }
    }
    public function clearAction(){
        $id = !empty($_GET['id']) ? $_GET['id'] : null;
        debug($id);
        if(isset($_SESSION['cart'])){
            $cart = new Cart();
            $cart->clearCart();
        }
    }
    public function viewAction(){
        $this->layout = 'cart-template';
        $this->setMeta('Оформлення замовлення');

    }
    public function checkoutAction(){
        if(!empty($_POST)){
            $cart = new Cart();
            $data['firstName'] = $_POST['firstname'];
            $data['lastName'] = $_POST['lastname'];
            $data['email'] = $_POST['email'];
            $data['phone'] = $_POST['telephone'];
            $data['price'] = $_POST['price'];
            $date = (string)date('Y-m-d H:i:s');
            \R::exec("INSERT INTO `Order` ( dateOfOrdering, firstName, lastName, email,phone,price )
                          VALUES( '{$date}','{$data['firstName']}','{$data['lastName']}','{$data['email']}','{$data['phone']}',{$data['price']})");

            $order = \R::findOne('Order','dateOfOrdering=?',[$date]);
            $orderId = $order->id;
            foreach ($_SESSION['cart'] as $item) {
                \R::exec("INSERT INTO `Order_Product`(`order_id`, `product_id`, `qty`, `title`, `price`)
                              VALUES ({$orderId},{$item['id']},{$item['qty']},'{$item['title']}',{$item['price']})");

            }
            $cart->clearCart();
            redirect();
        }
    }
}
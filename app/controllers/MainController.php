<?php


namespace app\controllers;



use internetshop\Cache;

class MainController extends AppController
{
    function __construct($route)
    {
        parent::__construct($route);

    }
    public function indexAction(){

        $categories = \R::find('Category','LIMIT 3');
        $phones = \R::getAll('SELECT * FROM Product WHERE CategoryId = 3');
        $phone_brands = \R::getAll('SELECT Brand.id,Brand.title FROM Brand  JOIN Product ON Brand.id = Product.BrandId');
        $tablets = \R::getAll('SELECT * FROM Product WHERE CategoryId = 4');
        $dell = \R::getAll('SELECT * FROM PRODUCT WHERE BrandId = 4');
        $hp = \R::getAll('SELECT * FROM PRODUCT WHERE BrandId = 5');
        $asus = \R::getAll('SELECT * FROM PRODUCT WHERE BrandId = 6');
        $samsung = \R::getAll('SELECT * FROM PRODUCT WHERE BrandId = 1');
        $lg = \R::getAll('SELECT * FROM PRODUCT WHERE BrandId = 2');
        $apple = \R::getAll('SELECT * FROM PRODUCT WHERE BrandId = 3');
        $laptops = \R::getAll('SELECT * FROM Product WHERE CategoryId = 5');
        $newest = \R::getAll('SELECT * FROM Product WHERE YearOfManufacturing>2018');
        $count = \R::getAll('SELECT COUNT(Id) AS Count FROM Product WHERE YearOfManufacturing>2018');

        $this->setMeta('Головна сторінка', 'Про сторінку', 'Ключові слова');
        $this->set(compact('categories','phones','phone_brands',
                                   'tablets','laptops','dell','hp','asus',
                                   'samsung','apple','lg','newest','count'));
    }
    public function cartAction(){
        $this->layout = 'cart-template';
        $this->setMeta('Кошик', 'Про сторінку', 'Ключові слова');
    }
    public function aboutAction(){

    }
}
<?php


namespace app\controllers;



class ProductController extends AppController
{
    public function viewAction(){
        $alias = $this->route['alias'];
        $product = \R::findOne('product','alias = ?',[$alias]);
        $colors = \R::getAll('SELECT * FROM Colors');
        $sizes = \R::getAll('SELECT * FROM Sizes');
        if(!$product){
            throw new \Exception('Товар не знайдено', 404);
        }
        $this->setMeta($product->model,$product->description,'Ключові слова');
        $this->set(compact('product','colors','sizes','gallery'));
    }

    public function categoriesAction(){
        $this->setMeta('QWD','','Ключові слова');
        $rare = $_GET['selectId'];
        debug($rare);
    }
}
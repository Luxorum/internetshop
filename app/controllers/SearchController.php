<?php


namespace app\controllers;



class SearchController extends AppController
{
    public function typeaheadAction(){
            if($this->isAjax()){
                $query = !empty(trim($_GET['query'])) ? trim($_GET['query']) : null;
                if($query){
                    $products = \R::getAll('SELECT id, model FROM Product WHERE model LIKE ? LIMIT 11', ["%{$query}%"]);
                    echo json_encode($products);
                }
            }
            die;

    }
    public function indexAction(){
        $query = !empty(trim($_GET['s'])) ? trim($_GET['s']) : null;
        if($query){
            $products = \R::find('Product', "model LIKE ?", ["%{$query}%"]);
        }
        $this->layout='cart-template';
        $this->setMeta('Пошук за: ' . h($query));
        $this->set(compact('products', 'query'));
    }
}
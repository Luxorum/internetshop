<?php


namespace app\controllers\admin;


use app\models\Product;
use app\models\AppModel;
use internetshop\libs\Pagination;

class ProductController extends AppController
{
    public function indexAction(){
        $productInstance = new Product();
        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $perpage = 10;
        $count = \R::count('product');
        $pagination = new Pagination($page, $perpage, $count);
        $start = $pagination->getStart();
        $products = $productInstance->getAllProducts($start,$perpage);
        $this->setMeta('Список товаров');
        $this->set(compact('products', 'pagination', 'count'));
    }
    public function addAction(){
        $productInstance = new Product();
        $brands = $productInstance->getBrands();
        $this->layout = 'admin';
        if(!empty($_POST)){
            $_POST['brandId'] = $productInstance->getBrand($_POST['brandId']);
            $brandId = $_POST['brandId'];
            $_POST['categoryId'] = $brandId[0]['categoryId'];
            $brandId = $brandId[0]['id'];
            $_POST['brandId'] = $brandId;
            $product = new Product();
            $_POST['hit'] = $_POST['hit']=='on' ? '1' : '0';

            $data = $_POST;
            $alias = AppModel::createAlias('product', 'alias', $data['model']);
            $data['alias'] = $alias;
            $product->addProduct($data);
            unset($data['model']);
            unset($data['yearOfManufacturing']);
            unset($data['brandId']);
            unset($data['price']);
            unset($data['categoryId']);
            unset($data['alias']);
            unset($data['hit']);
            unset($data['desc']);
            $lastprod = $product->getLastProduct();
            $lastprod = $lastprod[0]['id'];
            $product->setFilters($lastprod,$data);

//            $product->load($data);
//            $product->getImg();
//
//
//
//            if($id = $product->save('product')){
//                $product->saveGallery($id);
//                $p = $product->loadProduct($id);
//
//                $p->alias = $alias;
//                $product->storeProduct($p);
//                $product->editFilter($id, $data);
//                $_SESSION['success'] = 'Товар добавлен';
////            }

           redirect();
        }
        $this->set(compact('brands'));
        $this->setMeta('Новий товар');
    }
    public function deleteAction(){
        $productInstance = new Product();
        $product_id = $this->getRequestID();
        $product = $productInstance->loadProduct($product_id);
        $productInstance->deleteProduct($product);
        $_SESSION['success'] = 'Заказ видалено';
        redirect(ADMIN . '/product');
    }
}
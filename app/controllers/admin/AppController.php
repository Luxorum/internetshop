<?php


namespace app\controllers\admin;


use app\models\User;
use internetshop\base\Controller;

class AppController extends Controller
{
    public $layout = 'admin';

    public function __construct($route)
    {
        parent::__construct($route);
        if(!User::checkAdminAuth() && $route['action'] != 'login-admin'){
            redirect(ADMIN . '/user/login-admin');
        }

    }
    public function getRequestID($get = true, $id = 'id'){
        if($get){
            $data = $_GET;
        }else{
            $data = $_POST;
        }
        $id = !empty($data[$id]) ? (int)$data[$id] : null;
        if(!$id){
            throw new \Exception('Страница не найдена', 404);
        }
        return $id;
    }

}
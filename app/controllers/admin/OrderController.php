<?php


namespace app\controllers\admin;


use app\models\OrderModel;
use internetshop\libs\Pagination;

class OrderController extends AppController
{
    public function indexAction(){
        $order = new OrderModel();
        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $perpage = 5;
        $count = $order->getOrdersCount();
        $pagination = new Pagination($page, $perpage, $count);
        $start = $pagination->getStart();
        $orders = $order->getOrders($start,$perpage);
        $this->setMeta('Список замовлень');
        $this->set(compact('orders', 'pagination', 'count'));
    }
    public function viewAction(){
        $orderInstance = new OrderModel();
        $order_id = $this->getRequestID();
        $order = $orderInstance->getJoinOrder($order_id);
        if(!$order){
            throw new \Exception('Сторінку не знайдено',404);
        }
        $order_products = \R::findAll('order_product', "order_id = ?", [$order_id]);
        $this->setMeta("Заказ №{$order_id}");
        $this->set(compact('order', 'order_products'));
    }
    public function deleteAction(){
        $orderInstance = new OrderModel();
        $order_id = $this->getRequestID();
        $order = $orderInstance->loadOrder($order_id);
        $orderInstance->deleteOrder($order);
        $_SESSION['success'] = 'Заказ видалено';
        redirect(ADMIN . '/order');
    }
}
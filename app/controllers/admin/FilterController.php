<?php


namespace app\controllers\admin;



use app\models\FilterModel;

class FilterController extends AppController
{
    public function groupDeleteAction(){
        $filter = new FilterModel();
        $id = $this->getRequestID();
        $count = $filter->getCountGroups($id);
        if($count){
            $_SESSION['error'] = 'Удаление невозможно, в группе есть атрибуты';
            redirect();
        }
        $filter->deleteGroup($id);
        $_SESSION['success'] = 'Удалено';
        redirect(ADMIN . '/filter/attribute-group');
    }
    public function attributeDeleteAction(){
        $filter = new FilterModel();
        $id = $this->getRequestID();
        $filter->deleteAttribute($id);
        $_SESSION['success'] = 'Удалено';
        redirect();
    }

    public function attributeAction(){
        $filterInstance = new FilterModel();
        $attrs = $filterInstance->getAttribute();
        $this->setMeta('Фільтри');
        $this->set(compact('attrs'));
    }
    public function attributeGroupAction(){
        $filterInstance = new FilterModel();
        $attrs_group = $filterInstance->getGroupAttribute();
        $this->setMeta('Групи фильтрів');
        $this->set(compact('attrs_group'));
    }
    public function attributeAddAction(){
        if(!empty($_POST)){
            $attr = new FilterModel();
            $data = $_POST;
            $attr->addAttribute($data['value'],$data['attr_group_id']);

                $_SESSION['success'] = 'Атрибут добавлен';
            redirect(ADMIN . 'filter/attribute');

        }
        $filter = new FilterModel();
        $group = $filter->findAllAttributes();
        $this->setMeta('Новый фильтр');
        $this->set(compact('group'));
    }
    public function attributeEditAction(){
        $filter = new FilterModel();
        if(!empty($_POST)){
            $id = $this->getRequestID(false);
            $attr = new FilterModel();
            $data = $_POST;
            $attr->attributes['value'] = $data['value'];
            $attr->attributes['attribute_group_id'] = $data['attribute_group_id'];
            $attr->load($data);

            if($attr->update('attribute_value', $id)){
                $_SESSION['success'] = 'Изменения сохранены';
                redirect(ADMIN . 'filter/attribute');
            }
        }
        $id = $this->getRequestID();
        $attr = $filter->loadAttribute($id);
        $attrs_group = $filter->findAllAttributes();
        $this->setMeta('Редагування атрибута');
        $this->set(compact('attr', 'attrs_group'));
    }
    public function groupEditAction(){
        if(!empty($_POST)){
            $id = $this->getRequestID(false);
            $group = new FilterModel();
            $data = $_POST;
            $group->attributes['title'] = $data['title'];
            $group->load($data);
            if($group->update('attribute_group', $id)){
                $_SESSION['success'] = 'Изменения сохранены';
                redirect(ADMIN . '/filter/attribute-group');
            }
                $_SESSION['success'] = 'Изменения сохранены';
                redirect(ADMIN . '/filter/attribute-group');

        }
        $filter = new FilterModel();
        $id = $this->getRequestID();
        $group = $filter->loadGroup($id);
        $this->setMeta("Редагування групи {$group->title}");
        $this->set(compact('group'));
    }
    public function groupAddAction(){
        if(!empty($_POST)){
            $group = new FilterModel();
            $data = $_POST;
            $group->addGroup($data['title']);
                $_SESSION['success'] = 'Группа добавлена';
                redirect(ADMIN . '/filter/attribute-group');

        }
        $this->setMeta('Нова групa фільтрів');
    }
}
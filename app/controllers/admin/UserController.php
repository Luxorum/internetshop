<?php


namespace app\controllers\admin;


use app\models\User;
class UserController extends AppController
{
    public function loginAdminAction(){

        $this->layout = 'login';
        if(!empty($_POST)){
            $user = new User();
            $adminData = $user->getAdminData();
            if($adminData[0]['login'] == $_POST['login'] && $adminData[0]['pass'] == $_POST['pass']){
                $_SESSION['admin'] = 'admin';
                $_SESSION['admin_name'] = $adminData[0]['name'];
                redirect(ADMIN);
            }
            else{
                $_SESSION['error'] =  "Введено невірний логін/пароль";
            }
        }
        $this->set(compact('adminData'));
    }
    public function exitAction(){
        User::logOut();
        redirect(PATH);
    }
}
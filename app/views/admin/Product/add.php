<section class="content-header">
<h1>
    Новый товар
</h1>
<ol class="breadcrumb">
    <li><a href="<?=ADMIN;?>"><i class="fa fa-dashboard"></i> Главная</a></li>
    <li><a href="<?=ADMIN;?>/product">Список товаров</a></li>
    <li class="active">Новый товар</li>
</ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <form action="<?=ADMIN;?>/product/add" method="post" data-toggle="validator" id="add">
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <label for="title">Назва товара</label>
                            <input type="text" name="model" class="form-control" id="title" placeholder="Назва товара" value="<?php isset($_SESSION['form_data']['model']) ? h($_SESSION['form_data']['model']) : null; ?>" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>


                        <div class="form-group">
                            <label for="description">Бренд</label>
                            <select name="brandId">
                                <?foreach ($brands as $brand):?>
                                <option><?=$brand['title']?></option>
                                <?endforeach;?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="description">Рік створення</label>
                            <input type="text" name="yearOfManufacturing" class="form-control" id="year" placeholder="Рік створення" value="<?php isset($_SESSION['form_data']['yearOfManufacturing']) ? h($_SESSION['form_data']['yearOfManufacturing']) : null; ?>">
                        </div>

                        <div class="form-group">
                            <label for="description">Опис</label>
                            <input type="text" name="desc" class="form-control" id="description" placeholder="Опис" value="<?php isset($_SESSION['form_data']['description']) ? h($_SESSION['form_data']['description']) : null; ?>">
                        </div>

                        <div class="form-group has-feedback">
                            <label for="price">Ціна</label>
                            <input type="text" name="price" class="form-control" id="description" placeholder="Цена" pattern="^[0-9.]{1,}$" value="<?php isset($_SESSION['form_data']['price']) ? h($_SESSION['form_data']['price']) : null; ?>" required data-error="Допускаются цифры и десятичная точка">
                            <div class="help-block with-errors"></div>
                        </div>





                        <div class="form-group">
                            <label>
                                <input type="checkbox" name="hit"> Хит
                            </label>
                        </div>


                        <?php new \app\widgets\filter\Filter(null, WWW . '/filter/admin_filter_tpl.php'); ?>
                        <!--https://dcrazed.com/html5-jquery-file-upload-scripts/-->


                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">Добавить</button>
                    </div>
                </form>
                <?php if(isset($_SESSION['form_data'])) unset($_SESSION['form_data']); ?>
            </div>
        </div>
    </div>

</section>

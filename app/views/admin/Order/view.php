<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Замовлення №<?=$order['id'];?>

        <a href="<?=ADMIN;?>/order/delete?id=<?=$order['id'];?>" class="btn btn-danger btn-xs delete">Видалити</a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=ADMIN;?>"><i class="fa fa-dashboard"></i> Головна</a></li>
        <li><a href="<?=ADMIN;?>/order">Список замовлень</a></li>
        <li class="active">Замовлення №<?=$order['id'];?></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tbody>
                            <tr>
                                <td>Номер замовлення</td>
                                <td><?=$order['id'];?></td>
                            </tr>
                            <tr>
                                <td>Дата заказа</td>
                                <td><?=$order['dateOfOrdering'];?></td>
                            </tr>

                            <tr>
                                <td>Кількість позицій</td>
                                <td><?=count($order_products);?></td>
                            </tr>
                            <tr>
                                <td>Сума замовлення</td>
                                <td>$<?=$order['sum'];?></td>
                            </tr>
                            <tr>
                                <td>Покупець</td>
                                <td><? echo "{$order['firstName']} {$order['lastName']}";?></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <h3>Деталі замовлення</h3>
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Модель</th>
                                <th>Кількість</th>
                                <th>Ціна</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $qty = 0; foreach($order_products as $product): ?>
                                <tr>
                                    <td><?=$product->id;?></td>
                                    <td><?=$product->title;?></td>
                                    <td><?=$product->qty; $qty += $product->qty?></td>
                                    <td>$<?=number_format($product->price,2);?></td>
                                </tr>
                            <?php endforeach; ?>
                            <tr class="active">
                                <td colspan="2">
                                    <b>Загалом:</b>
                                </td>
                                <td><b><?=$qty;?></b></td>
                                <td><b>$<?=$order['sum'];?> </b></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->

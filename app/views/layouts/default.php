<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    <?=$this->getMeta();?>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/png" href="/public/img/favicon.png">

    <link href='https://fonts.googleapis.com/css?family=Raleway:400,600' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Bootstrap CSS
    ============================================ -->
    <link rel="stylesheet" href="/public/css/bootstrap.min.css">

    <!-- Font awesome CSS
    ============================================ -->
    <link rel="stylesheet" href="/public/css/font-awesome.min.css">
    <!-- owl.carousel CSS
    ============================================ -->
    <link rel="stylesheet" href="/public/css/owl.carousel.css">
    <link rel="stylesheet" href="/public/css/owl.theme.css">
    <link rel="stylesheet" href="/public/css/owl.transitions.css">
    <!-- nivo slider CSS
    ============================================ -->
    <link rel="stylesheet" href="/public/css/nivo-slider.css" type="text/css" />
    <!-- meanmenu CSS
    ============================================ -->
    <link rel="stylesheet" href="/public/css/meanmenu.min.css">
    <!-- jquery-ui CSS
    ============================================ -->
    <link rel="stylesheet" href="/public/css/jquery-ui.css">
    <!-- animate CSS
    ============================================ -->
    <link rel="stylesheet" href="/public/css/animate.css">
    <!-- main CSS
    ============================================ -->
    <link rel="stylesheet" href="/public/css/main.css">
    <!-- style CSS
    ============================================ -->
    <link rel="stylesheet" href="/public/css/style.css">
    <!-- responsive CSS
    ============================================ -->
    <link rel="stylesheet" href="/public/css/responsive.css">

    <script>
        var path = '<?=PATH;?>';
    </script>
</head>
<body>

<!-- HEADER-AREA START -->
<header class="header-area">
    <!-- HEADER-TOP START -->
    <div class="header-top hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="top-menu">
                        <!-- Start Language -->

                        <!-- End Language -->
                        <!-- Start Currency -->

                        <!-- End Currency -->
                        <p class="welcome-msg">Вітаємо на нашому сайті!</p>
                    </div>
                    <!-- Start Top-Link -->
                    <div class="top-link">
                        <ul class="link">
                            <li><a href="/cart/view"><i class="fa fa-share"></i> Оформити замовлення</a></li>
                            <?if(!isset($_SESSION['admin'])):?>
                            <li><a href="/admin"><i class="fa fa-unlock-alt"></i> Увійти</a></li>
                            <?else:?>
                                <li><a href="/admin"><i class="fa fa-user"></i> Панель керування</a></li>
                            <?endif;?>
                        </ul>
                    </div>
                    <!-- End Top-Link -->
                </div>
            </div>
        </div>
    </div>
    <!-- HEADER-TOP END -->
    <!-- HEADER-MIDDLE START -->
    <div class="header-middle">
        <div class="container">
            <!-- Start Support-Client -->
            <div class="support-client hidden-xs">
                <div class="row">
                    <!-- Start Single-Support -->
                    <div class="col-md-3 hidden-sm">
                        <div class="single-support">
                            <div class="support-content">
                                <i class="fa fa-clock-o"></i>
                                <div class="support-text">
                                    <h1 class="zero gfont-1">Робочий час</h1>
                                    <p>Пн - Нд: 8.00 - 18.00</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single-Support -->
                    <!-- Start Single-Support -->
                    <div class="col-md-3 col-sm-4">
                        <div class="single-support">
                            <i class="fa fa-truck"></i>

                            <div class="support-text">
                                <h1 class="zero gfont-1">Безкоштовна доставка</h1>
                                <p>Для товарів дорожчих ніж 200$</p>
                            </div>
                        </div>
                    </div>
                    <!-- End Single-Support -->
                    <!-- Start Single-Support -->
                    <div class="col-md-3 col-sm-4">
                        <div class="single-support">
                            <i class="fa fa-money"></i>
                            <div class="support-text">
                                <h1 class="zero gfont-1">Гарантія повернення грошей 100%</h1>
                                <p>Протягом 30 днів з дня доставки</p>
                            </div>
                        </div>
                    </div>
                    <!-- End Single-Support -->
                    <!-- Start Single-Support -->
                    <div class="col-md-3 col-sm-4">
                        <div class="single-support">
                            <i class="fa fa-phone-square"></i>
                            <div class="support-text">
                                <h1 class="zero gfont-1">Телефон: +380991234567</h1>
                                <p>Замовляйте онлайн зараз!</p>
                            </div>
                        </div>
                    </div>
                    <!-- End Single-Support -->
                </div>
            </div>
            <!-- End Support-Client -->
            <!-- Start logo & Search Box -->
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="logo">
                        <a href="<?=PATH?>" title="Malias"><img src="/public/img/logo.png" alt="Malias"></a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-12">
                    <div class="quick-access">
                        <div class="search-by-category">
                            <div class="header-search">
                                <form action="search" method="get" autocomplete="off">
                                    <input id="typeahead" class="typeahead" name="s" type="text" placeholder="Пошук...">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="top-cart">
                            <ul>
                                <li>
                                    <?if(isset($_SESSION['cart']) && $_SESSION['cart.qty']!=0):?>
                                    <a href="/main/cart">
                                        <?else:?>
                                        <a href="" onclick="window.location.reload();">
                                            <?endif;?>
                                        <span class="cart-icon"><i class="fa fa-shopping-cart"></i></span>
                                        <span class="cart-total">
			                    					<span class="cart-title">Кошик</span>
                                            <?if(isset($_SESSION['cart']) && $_SESSION['cart.qty']!=0):?>
				                    				<span class="cart-item"><?=$_SESSION['cart.qty'];?> тов. -  </span>
				                    				<span class="top-cart-price">$<?=number_format($_SESSION['cart.sum'],2);?></span>
                                            <?else: ?>
                                                <p class='text-muted'>Кошик порожній</p>
                                            <?endif;?>
			                    				</span>
                                    </a>
                                    <div class="mini-cart-content">
                                        <?if(isset($_SESSION['cart']) && $_SESSION['cart.sum']!=0):?>
                                        <?foreach ($_SESSION['cart'] as $id => $item): ?>
                                    <div class="cart-content">
                                        <div class="cart-img-details">
                                            <div class="cart-img-photo">
                                                <a href="/product/<?=$item['alias']?>"><img src="/public/img/product/<?=$item['img']?>" alt="#"></a>
                                            </div>
                                            <div class="cart-img-content">
                                                <a href="/product/<?=$item['alias']?>"><h4><?=$item['title']?></h4></a>
                                                <span>
															<strong class="text-right"><?=$item['qty'];?> x</strong>
															<strong class="cart-price text-right">$<?=number_format($item['price'],2);?></strong>
														</span>
                                            </div>
                                            <div class="pro-del">
                                                <a class="delete-cart" data-id="<?=$id;?>" "><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                       <?endforeach;?>

                                        <div class="cart-inner-bottom">
													<span class="total">
														Усього: <?=$_SESSION['cart.qty'];?> одиниць товару на суму в
														<span class="amount">$<?=number_format($_SESSION['cart.sum'],2);?></span>
													</span>
                                            <span class="cart-button-top">
														<a href="/main/cart">Детальніше</a>
														<a href="/cart/view">Купити</a>
                                                        <a href="#" id="clear-cart" data-id="1">Очистити</a>
													</span>
                                        </div>
                                        <?endif;?>
                                        <?if(!isset($_SESSION['cart'])||$_SESSION['cart.sum']==0) echo "<p style='text-align: center' class='text-muted'>Кошик порожній</p>"?>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End logo & Search Box -->
        </div>
    </div>
    <!-- HEADER-MIDDLE END -->
    <!-- START MAINMENU-AREA -->
    <div class="mainmenu-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mainmenu hidden-sm hidden-xs">
                        <nav>
                            <ul>
                                <li><a href="<?=PATH?>">На головну</a></li>
                                <li><a href="/main/about">Про нас</a></li>
                                <li class="hot"><a href="/category/all">Найкращий вибір</a></li>
                                <li class="new"><a href="/category/all">Нові продукти</a></li>
                                <li><a href="/category/all">Спеціальні пропозиції</a></li>
                                <li><a href="/main/about">Зворотній зв'язок</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN-MENU-AREA -->
    <!-- Start Mobile-menu -->
    <div class="mobile-menu-area hidden-md hidden-lg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <nav id="mobile-menu">
                        <ul>
                            <li><a href="<?=PATH?>">На головну</a></li>
                            <li><a href="about.html">Про нас</a></li>
                            <li class="hot"><a href="/category/all">Найкращий вибір</a></li>
                            <li class="new"><a href="/category/all">Нові продукти</a></li>
                            <li><a href="/category/all">Спеціальні пропозиції</a></li>
                            <li><a href="contact.html">Зворотній зв'язок</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="category-slider-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- CATEGORY-MENU-LIST START -->
                    <div class="left-category-menu">
                        <div class="left-product-cat">
                            <div class="category-heading">
                                <h2>Категорії</h2>
                            </div>
                            <div class="category-menu-list">
                                <ul>
                                    <li><a href="/category/all">Усі</a></li>
                                    <li><a href="/category/phones">Телефони</a></li>
                                    <li><a href="/category/tablets">Планшети</a></li>
                                    <li><a href="/category/laptops">Ноутбуки</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- End Mobile-menu -->
</header>

<?=$content?>


<div class="subscribe-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-7 col-xs-12">
                <label class="hidden-sm hidden-xs">Підпишись на нас:</label>
                <div class="subscribe">
                    <form action="#">
                        <input type="text" placeholder="Введи свій E-mail">
                        <button type="submit">Підписатись</button>
                    </form>
                </div>
            </div>
            <div class="col-md-4 col-sm-5 col-xs-12">
                <div class="social-media">
                    <a href="#"><i class="fa fa-facebook fb"></i></a>
                    <a href="#"><i class="fa fa-google-plus gp"></i></a>
                    <a href="#"><i class="fa fa-twitter tt"></i></a>
                    <a href="#"><i class="fa fa-youtube yt"></i></a>
                    <a href="#"><i class="fa fa-linkedin li"></i></a>
                    <a href="#"><i class="fa fa-rss rs"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SUBSCRIBE-AREA -->

<!-- END HOME-PAGE-CONTENT -->
<!-- FOOTER-AREA START -->
<footer class="footer-area">
    <!-- Footer Start -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="footer-title">
                        <h5>My Account</h5>
                    </div>
                    <nav>
                        <ul class="footer-content">
                            <li><a href="my-account.html">Мій аккаунт</a></li>
                            <li><a href="#">Order History</a></li>
                            <li><a href="wishlist">Wish List</a></li>
                            <li><a href="#">Search Terms</a></li>
                            <li><a href="#">Returns</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="footer-title">
                        <h5>Customer Service</h5>
                    </div>
                    <nav>
                        <ul class="footer-content">
                            <li><a href="contact.html">Contact Us</a></li>
                            <li><a href="about.html">About Us</a></li>
                            <li><a href="#">Delivery Information</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 hidden-sm col-md-3">
                    <div class="footer-title">
                        <h5>Payment & Shipping</h5>
                    </div>
                    <nav>
                        <ul class="footer-content">
                            <li><a href="#">Brands</a></li>
                            <li><a href="#">Gift Vouchers</a></li>
                            <li><a href="#">Affiliates</a></li>
                            <li><a href="shop-list.html">Specials</a></li>
                            <li><a href="#">Search Terms</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="footer-title">
                        <h5>Payment & Shipping</h5>
                    </div>
                    <nav>
                        <ul class="footer-content box-information">
                            <li>
                                <i class="fa fa-home"></i><span>Towerthemes, 1234 Stret Lorem, LPA States, Libero</span>
                            </li>
                            <li>
                                <i class="fa fa-envelope-o"></i><p><a href="contact.html">admin@bootexperts.com</a></p>
                            </li>
                            <li>
                                <i class="fa fa-phone"></i>
                                <span>01234-56789</span> <br> <span> 01234-56789</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End -->
    <!-- Copyright-area Start -->
    <div class="copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p>Copyright &copy;ZSTU PI-56 All rights reserved.</p>
                        <div class="payment">
                            <a href="#"><img src="/public/img/payment.png" alt="Payment"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/public/js/jquery-1.11.3.min.js"></script>

    <script src="/public/js/main.js"></script>
    <!-- bootstrap JS
    ============================================ -->
    <script src="/public/js/bootstrap.min.js"></script>
    <!-- wow JS
    ============================================ -->
    <script src="/public/js/wow.min.js"></script>
    <!-- meanmenu JS
    ============================================ -->
    <script src="/public/js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
    ============================================ -->
    <script src="/public/js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
    ============================================ -->
    <script src="/public/js/jquery.scrollUp.min.js"></script>
    <!-- countdon.min JS
    ============================================ -->
    <script src="/public/js/countdon.min.js"></script>
    <!-- jquery-price-slider js
    ============================================ -->
    <script src="/public/js/jquery-price-slider.js"></script>
    <!-- Nivo slider js
    ============================================ -->
    <script src="/public/js/jquery.nivo.slider.js" type="text/javascript"></script>
    <!-- plugins JS
    ============================================ -->
    <script src="/public/js/plugins.js"></script>
    <!-- main JS
    ============================================ -->

    <script src="/public/js/typeahead.bundle.js"></script>
    <script src="/public/js/main.js"></script>
    <!-- Copyright-area End -->

</footer>

<!-- FOOTER-AREA END -->
<!-- QUICKVIEW PRODUCT -->


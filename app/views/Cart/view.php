<div class="col-md-9">
    <div class="product-banner">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner">
                    <a href="#"><img src="/public/img/banner/12.jpg" alt="Product Banner"></a>
                </div>
            </div>
        </div>
    </div>
    <?if(isset($_SESSION['cart'])):?>
    <?if(!empty($_SESSION['cart'] && $_SESSION['cart.qty']!==0)):?>


    <div class="checkout-area">
        <div class="row">
            <div class="col-md-12">

                <div class="cart-title">
                    <h2 class="entry-title">Замовлення</h2>
                </div>
                <form method="post" action="/cart/checkout">
                <div class="panel-group" id="accordion">

                    <div class="panel panel_default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-trigger  collapsed" id="form1" data-toggle="collapse" data-parent="#accordion" href="#payment-address">Крок 1: Персональні дані <i class="fa fa-caret-down"></i> </a>
                            </h4>
                        </div>
                        <div id="payment-address" class="collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <fieldset id="account">
                                            <legend>Ваші персональні дані</legend>
                                            <div class="form-group">
                                                <label><sup>*</sup>Ім'я</label>
                                                <input type="text" required class="form-control" placeholder="First Name" name="firstname" />
                                            </div>
                                            <div class="form-group">
                                                <label><sup>*</sup>Прізвище</label>
                                                <input type="text" required class="form-control" placeholder="Last Name" name="lastname" />
                                            </div>
                                            <div class="form-group">
                                                <label><sup>*</sup>E-mail</label>
                                                <input type="email" required class="form-control" placeholder="E-mail" name="email" />
                                            </div>
                                            <div class="form-group">
                                                <label><sup>*</sup>Телефон</label>
                                                <input type="tel" required class="form-control" placeholder="Telephone" name="telephone" />
                                            </div>

                                        </fieldset>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="panel panel_default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-trigger collapsed" data-toggle="collapse" data-parent="#accordion" href="#checkout-confirm">Крок 2: Підтвердити замовлення <i class="fa fa-caret-down"></i> </a>
                            </h4>
                        </div>
                        <div id="checkout-confirm" class="collapse">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <td class="text-left">Модель продукту</td>
                                            <td class="text-left">Кількість</td>
                                            <td class="text-left">Ціна за од. товару</td>
                                            <td class="text-left">Усього</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?foreach ($_SESSION['cart'] as $item):?>
                                        <tr>
                                            <td class="text-left">
                                                <a href="#"><?=$item['title']?></a>
                                            </td>
                                            <td class="text-left"><?=$item['qty']?></td>
                                            <td class="text-left">$<?=number_format($item['price'],2)?></td>
                                            <td class="text-left">$<?=number_format($item['qty']*$item['price'],2)?></td>
                                        </tr>
                                        <?endforeach;?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td class="text-right" colspan="3">
                                                <strong>Загальна кількість:</strong>
                                            </td>
                                            <td class="text-right"><?=$_SESSION['cart.qty']?></td>
                                        </tr>
                                        <tr>
                                            <td class="text-right" colspan="3">
                                                <strong>Загальна вартість:</strong>
                                            </td>
                                            <td class="text-right">$<?=number_format($_SESSION['cart.sum'],2)?></td>
                                        </tr>
                                        <tr>
                                            <td class="text-right" colspan="3">
                                                <strong>Вартість доставки:</strong>
                                            </td>
                                            <td class="text-right">$5.00</td>
                                        </tr>

                                        </tfoot>
                                    </table>
                                </div>
                                <input type="hidden" name="price" value="<?=$_SESSION['cart.sum']+5?>">
                                <div class="buttons pull-right">
                                    <input type="submit" class="btn btn-primary" onclick="validate();" value="Підтвердити" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <?else:?>
            <p style="font-size: 18px;" class="text-muted">Кошик порожній</p>
    <?endif;?>
    <?else:?>
        <p style="font-size: 18px;" class="text-muted">Кошик порожній</p>
    <?endif;?>
    <!-- End Shopping-Cart -->
</div>

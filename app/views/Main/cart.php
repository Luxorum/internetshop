<div class="col-md-9">

    <!-- END PRODUCT-BANNER -->
    <!-- Start Shopping-Cart -->
    <div class="shopping-cart">
        <div class="row">
            <div class="col-md-12">
                <div class="cart-title">
                    <h2 class="entry-title">Shopping Cart</h2>
                </div>
                <!-- Start Table -->
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td class="text-center">Фото</td>
                            <td class="text-left">Модель</td>
                            <td class="text-left">Кількість</td>
                            <td class="text-right">Ціна за од. товару</td>
                            <td class="text-right">Усього</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?foreach ($_SESSION['cart'] as $item):?>
                        <tr>
                            <td class="text-center">
                                <a href="/product/<?=$item['alias']?>"><img class="img-thumbnail" style="max-width: 100px; max-height: 100px;" src="/public/img/product/<?=$item['img']?>" alt="#" /></a>
                            </td>
                            <td class="text-left">
                                <a href="/product/<?=$item['alias']?>"><?=$item['title']?></a>
                            </td>
                            <td class="text-left">
                                <div class="btn-block cart-put">
                                    <p><?=$item['qty']?></p>

                                </div>
                            </td>
                            <td class="text-right">$<?=number_format($item['price'],2)?></td>
                            <td class="text-right">$<?=number_format($item['price'] * $item['qty'],2)?></td>
                        </tr>
                        <?endforeach;?>

                        </tbody>
                    </table>
                </div>
                <!-- End Table -->

                <!-- Accordion end -->
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-8">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td class="text-right">
                                    <strong>Загальна кількість:</strong>
                                </td>
                                <td class="text-right"><?=$_SESSION['cart.qty'];?></td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <strong>Усього:</strong>
                                </td>
                                <td class="text-right">$<?=number_format($_SESSION['cart.sum'],2)?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="shopping-checkout">
                    <a href="/category/all" class="btn btn-default pull-left " style="margin-bottom: 10px;font-size:15px">Продовжити покупки</a>
                    <a href="/cart/view" class="btn btn-primary pull-right" style="font-size:15px;">Купити</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Shopping-Cart -->
</div>
<div></div>
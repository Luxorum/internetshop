

<!-- HEADER AREA END -->
<!-- Category slider area start -->

<!-- Category slider area End -->
<!-- START PAGE-CONTENT -->
<section class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <!-- START HOT-DEALS-AREA -->
                <div class="hot-deals-area carosel-circle">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="area-title">
                                <h3 class="title-group border-red gfont-1">Гарячі знижки</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="active-hot-deals">
                            <!-- Start Single-hot-deals -->
                            <div class="col-xs-12">
                                <div class="single-hot-deals">
                                    <div class="hot-deals-photo">
                                        <a href="#"><img src="/public/img/hot-deals/1.jpg" alt="Product"></a>
                                    </div>
                                    <div class="count-down">
                                        <div class="timer">
                                            <div data-countdown="2019/06/15"></div>
                                        </div>
                                    </div>
                                    <div class="hot-deals-text">
                                        <h5><a href="#" class="name-group">Various Versions</a></h5>
                                        <span class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i></span>
                                        <div class="price-box">
                                            <span class="price gfont-2">$99.00</span>
                                            <span class="old-price gfont-2">$110.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-hot-deals -->
                            <!-- Start Single-hot-deals -->
                            <div class="col-xs-12">
                                <div class="single-hot-deals">
                                    <div class="hot-deals-photo">
                                        <a href="#"><img src="/public/img/hot-deals/2.jpg" alt="Product"></a>
                                    </div>
                                    <div class="count-down">
                                        <div class="timer">
                                            <div data-countdown="2017/06/30"></div>
                                        </div>
                                    </div>
                                    <div class="hot-deals-text">
                                        <h5><a href="#" class="name-group">Trid Palm</a></h5>
                                        <span class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i></span>
                                        <div class="price-box">
                                            <span class="price gfont-2">$85.00</span>
                                            <span class="old-price gfont-2">$120.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-hot-deals -->
                            <!-- Start Single-hot-deals -->
                            <div class="col-xs-12">
                                <div class="single-hot-deals">
                                    <div class="hot-deals-photo">
                                        <a href="#"><img src="/public/img/hot-deals/3.jpg" alt="Product"></a>
                                    </div>
                                    <div class="count-down">
                                        <div class="timer">
                                            <div data-countdown="2017/08/30"></div>
                                        </div>
                                    </div>
                                    <div class="hot-deals-text">
                                        <h5><a href="#" class="name-group">Established Fact</a></h5>
                                        <span class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i></span>
                                        <div class="price-box">
                                            <span class="price gfont-2">$90.00</span>
                                            <span class="old-price gfont-2">$105.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-hot-deals -->
                        </div>
                    </div>
                </div>
                <!-- END HOT-DEALS-AREA -->
                <!-- START SMALL-PRODUCT-AREA -->
                <div class="small-product-area carosel-navigation">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="area-title">
                                <h3 class="title-group gfont-1">Bestseller</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="active-bestseller sidebar">
                            <div class="col-xs-12">
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="/public/img/product/small/1.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Various Versions</a></h5>
                                        <div class="price-box">
                                            <span class="price">$99.00</span>
                                            <span class="old-price">$120.00</span>
                                        </div>
                                        <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="/public/img/product/small/2.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Established Fact</a></h5>
                                        <div class="price-box">
                                            <span class="price">$85.00</span>
                                            <span class="old-price">$105.00</span>
                                        </div>
                                        <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="/public/img/product/small/3.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Trid Palm</a></h5>
                                        <div class="price-box">
                                            <span class="price">$90.00</span>
                                            <span class="old-price">$120.00</span>
                                        </div>
                                        <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="/public/img/product/small/4.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Established Fact</a></h5>
                                        <div class="price-box">
                                            <span class="price">$85.00</span>
                                            <span class="old-price">$105.00</span>
                                        </div>
                                        <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                            </div>
                            <div class="col-xs-12">
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="/public/img/product/small/5.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Various Versions</a></h5>
                                        <div class="price-box">
                                            <span class="price">$99.00</span>
                                            <span class="old-price">$120.00</span>
                                        </div>
                                        <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="/public/img/product/small/6.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Established Fact</a></h5>
                                        <div class="price-box">
                                            <span class="price">$85.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="/public/img/product/small/7.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Trid Palm</a></h5>
                                        <div class="price-box">
                                            <span class="price">$90.00</span>
                                            <span class="old-price">$120.00</span>
                                        </div>
                                        <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="/public/img/product/small/8.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Established Fact</a></h5>
                                        <div class="price-box">
                                            <span class="price">$85.00</span>
                                            <span class="old-price">$105.00</span>
                                        </div>
                                        <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                            </div>
                        </div>
                    </div>



                </div>
                <!-- END SMALL-PRODUCT-AREA -->
                <!-- START SIDEBAR-BANNER -->
                <div class="sidebar-banner">
                    <div class="active-sidebar-banner">
                        <div class="single-sidebar-banner">
                            <a href="#"><img src="/public/img/banner/1.jpg" alt="Product Banner"></a>
                        </div>
                        <div class="single-sidebar-banner">
                            <a href="#"><img src="/public/img/banner/2.jpg" alt="Product Banner"></a>
                        </div>
                    </div>
                </div>
                <!-- END SIDEBAR-BANNER -->
                <!-- START RECENT-POSTS -->

                <!-- END RECENT-POSTS -->
            </div>
            <div class="col-md-9 col-sm-9">
                <!-- START PRODUCT-BANNER -->
                <div class="product-banner home1-banner">
                    <div class="row">
                        <div class="col-md-7 banner-box1">
                            <div class="single-product-banner">
                                <a href="#"><img src="/public/img/banner/3.jpg" alt="Product Banner"></a>
                                <div class="banner-text banner-1">
                                    <h2>head phone 2015</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 banner-box2">
                            <div class="single-product-banner">
                                <a href="#"><img src="/public/img/banner/4.jpg" alt="Product Banner"></a>
                                <div class="banner-text banner-2">
                                    <h2>Deals <span>50%</span></h2>
                                    <p>lumina n85</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PRODUCT-BANNER -->
                <!-- START PRODUCT-AREA (1) -->
                <div class="product-area">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <!-- Start Product-Menu -->
                            <div class="product-menu">
                                <div class="product-title">
                                    <h3 class="title-group-2 gfont-1">Електроніка</h3>
                                </div>

                                <ul role="tablist">
                                    <?php
                                    $i = 0;
                                    foreach($categories as $category): ?>
                                    <?$i++;?>
                                    <li role="presentation" class="<? if($i===1) echo 'active';?>"><a href="#display-1-<?=$i?>" role="tab" data-toggle="tab"><?=$category->name?></a></li>

                                    <?php endforeach;?>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <!-- End Product-Menu -->
                    <div class="clear"></div>
                    <!-- Start Product -->
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="product carousel-navigation">
                                <div class="tab-content">
                                    <!-- Product = display-1-1 -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="display-1-1">
                                        <div class="row">
                                            <div class="active-product-carosel">
                                                <!-- Start Single-Product -->
                                                <? foreach($phones as $phone):
                                                    ?>
                                                <div class="col-xs-12">
                                                    <div class="single-product">
                                                        <? if ($phone['hit']==='1') echo
                                                        "<div class=\"label_new\">
                                                            <span class=\"new\">new</span>
                                                        </div>"
                                                        ?>
                                                        <div class="sale-off">
                                                            <span class="sale-percent">-55%</span>
                                                        </div>
                                                        <div class="product-img">
                                                            <a href="product/<?=$phone['alias']?>">
                                                                <img class="primary-img" src="/public/img/product/<?=$phone['img']?>" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">
                                                                    <? foreach ($phone_brands as $brand):
                                                                        if($brand['id']===$phone['brandId']){
                                                                            echo "{$brand['title']} ";
                                                                            break;
                                                                        } ?>
                                                                    <? endforeach;?>
                                                                    <?=$phone['model']?></a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$<?=number_format($phone['price'],2)?></span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                        </div>
                                                        <div class="product-action">
                                                            <div class="button-group">
                                                                <div class="product-button">
                                                                        <a class="toch-button toch-add-cart add-to-cart" data-id="<?=$phone['id']?>" href="/cart/add?id=<?=$phone['id']?>" "><i class="fa fa-shopping-cart"></i> У кошик</a>
                                                                </div>
                                                                <div class="product-button-2">
                                                                    <a href="#" class="modal-view" data-toggle="modal" data-target="#productModal-1-<?=$phone['id']?>"><i class="fa fa-search-plus"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <? endforeach;?>
                                                <!-- End Single-Product -->
                                                <!-- Start Single-Product -->


                                                <!-- End Single-Product -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-1-1 -->
                                    <!-- Start Product = display-1-2 -->
                                    <div role="tabpanel" class="tab-pane fade" id="display-1-2">
                                        <div class="row">
                                            <div class="active-product-carosel">
                                                <!-- Start Single-Product -->
                                                <? foreach($tablets as $tablet):
                                                    ?>
                                                    <div class="col-xs-12">
                                                        <div class="single-product">
                                                            <? if ($tablet['hit']==='1') echo
                                                            "<div class=\"label_new\">
                                                            <span class=\"new\">new</span>
                                                        </div>"
                                                            ?>
                                                            <div class="sale-off">
                                                                <span class="sale-percent">-55%</span>
                                                            </div>
                                                            <div class="product-img">
                                                                <a href="product/<?=$tablet['alias']?>">
                                                                    <img class="primary-img" src="/public/img/product/<?=$tablet['img']?>" alt="Product">
                                                                </a>
                                                            </div>
                                                            <div class="product-description">
                                                                <h5><a href="#">
                                                                        <? foreach ($phone_brands as $brand):
                                                                            if($brand['id']===$tablet['brandId']) echo "{$brand['title']} ";?>
                                                                        <? endforeach;?>
                                                                        <?=$tablet['model']?></a></h5>
                                                                <div class="price-box">
                                                                    <span class="price">$<?=number_format($tablet['price'],2)?></span>
                                                                </div>
                                                                <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            </div>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button">
                                                                        <a class="toch-button toch-add-cart add-to-cart" data-id="<?=$tablet['id']?>" href="/cart/add?id=<?=$tablet['id']?>" "><i class="fa fa-shopping-cart"></i> У кошик</a>
                                                                    </div>
                                                                    <div class="product-button-2">
                                                                        <a href="#" class="modal-view" data-toggle="modal" data-target="#productModal-2-<?=$tablet['id']?>"><i class="fa fa-search-plus"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endforeach;?>
                                                <!-- End Single-Product -->
                                                <!-- Start Single-Product -->


                                                <!-- End Single-Product -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-1-2 -->
                                    <!-- Start Product = di3play-1-1 -->
                                    <div role="tabpanel" class="tab-pane fade" id="display-1-3">
                                        <div class="row">
                                            <div class="active-product-carosel">
                                                <!-- Start Single-Product -->
                                                <? foreach($laptops as $laptop):
                                                    ?>
                                                    <div class="col-xs-12">
                                                        <div class="single-product">
                                                            <? if ($laptop['hit']==='1') echo
                                                            "<div class=\"label_new\">
                                                            <span class=\"new\">new</span>
                                                        </div>"
                                                            ?>
                                                            <div class="sale-off">
                                                                <span class="sale-percent">-55%</span>
                                                            </div>
                                                            <div class="product-img">
                                                                <a href="product/<?=$laptop['alias']?>">
                                                                    <img class="primary-img" src="/public/img/product/<?=$laptop['img']?>" alt="Product">
                                                                </a>
                                                            </div>
                                                            <div class="product-description">
                                                                <h5><a href="#">
                                                                        <? foreach ($phone_brands as $brand):
                                                                            if($brand['id']===$laptop['brandId']) echo "{$brand['title']} ";?>
                                                                        <? endforeach;?>
                                                                        <?=$laptop['model']?></a></h5>
                                                                <div class="price-box">
                                                                    <span class="price">$<?=number_format($laptop['price'],2)?></span>
                                                                </div>
                                                                <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            </div>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button">
                                                                        <a class="toch-button toch-add-cart add-to-cart" data-id="<?=$laptop['id']?>" href="/cart/add?id=<?=$laptop['id']?>" "><i class="fa fa-shopping-cart"></i> У кошик</a>
                                                                    </div>
                                                                    <div class="product-button-2">
                                                                        <a href="#" class="modal-view" data-toggle="modal" data-target="#productModal-3-<?=$laptop['id']?>"><i class="fa fa-search-plus"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endforeach;?>
                                                <!-- End Single-Product -->
                                                <!-- Start Single-Product -->


                                                <!-- End Single-Product -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-1-3 -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Product -->
                </div>
                <!-- END PRODUCT-AREA (1) -->
                <!-- START PRODUCT-AREA (2) -->
                <div class="product-area">
                    <!-- Start Product-Menu -->
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="product-menu  border-red">
                                <div class="product-title">
                                    <h3 class="title-group-2 gfont-1">Laptops</h3>
                                </div>

                                <ul role="tablist">
                                    <li role="presentation" class=" active"><a href="#display-2-1" role="tab" data-toggle="tab">Dell</a></li>
                                    <li role="presentation"><a href="#display-2-2" role="tab" data-toggle="tab">Hp</a></li>
                                    <li role="presentation"><a href="#display-2-4"  role="tab" data-toggle="tab">Asus</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Product-Menu -->
                    <!-- Start Product -->
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="product carosel-navigation">
                                <div class="tab-content">
                                    <!-- Start Product = display-2-1 -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="display-2-1">
                                        <div class="row">
                                            <div class="active-product-carosel">
                                                <!-- Start Single-Product -->
                                                <? foreach($dell as $laptop):
                                                    ?>
                                                    <div class="col-xs-12">
                                                        <div class="single-product">
                                                            <? if ($laptop['hit']==='1') echo
                                                            "<div class=\"label_new\">
                                                            <span class=\"new\">new</span>
                                                        </div>"
                                                            ?>
                                                            <div class="sale-off">
                                                                <span class="sale-percent">-55%</span>
                                                            </div>
                                                            <div class="product-img">
                                                                <a href="product/<?=$laptop['alias']?>">
                                                                    <img class="primary-img" src="/public/img/product/<?=$laptop['img']?>" alt="Product">
                                                                </a>
                                                            </div>
                                                            <div class="product-description">
                                                                <h5><a href="#">
                                                                        <? foreach ($phone_brands as $brand):
                                                                            if($brand['id']===$laptop['brandId']) echo "{$brand['title']} ";?>
                                                                        <? endforeach;?>
                                                                        <?=$laptop['model']?></a></h5>
                                                                <div class="price-box">
                                                                    <span class="price">$<?=number_format($laptop['price'],2)?></span>
                                                                </div>
                                                                <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            </div>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button">
                                                                        <a class="toch-button toch-add-cart add-to-cart" data-id="<?=$laptop['id']?>" href="/cart/add?id=<?=$laptop['id']?>" "><i class="fa fa-shopping-cart"></i> У кошик</a>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endforeach;?>
                                                <!-- End Single-Product -->
                                                <!-- Start Single-Product -->


                                                <!-- End Single-Product -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-2-1 -->
                                    <!-- Start Product = display-2-2 -->
                                    <div role="tabpanel" class="tab-pane fade" id="display-2-2">
                                        <div class="row">
                                            <div class="active-product-carosel">
                                                <!-- Start Single-Product -->
                                                <? foreach($hp as $laptop):
                                                    ?>
                                                    <div class="col-xs-12">
                                                        <div class="single-product">
                                                            <? if ($laptop['hit']==='1') echo
                                                            "<div class=\"label_new\">
                                                            <span class=\"new\">new</span>
                                                        </div>"
                                                            ?>
                                                            <div class="sale-off">
                                                                <span class="sale-percent">-55%</span>
                                                            </div>
                                                            <div class="product-img">
                                                                <a href="product/<?=$laptop['alias']?>">
                                                                    <img class="primary-img" src="/public/img/product/<?=$laptop['img']?>" alt="Product">
                                                                </a>
                                                            </div>
                                                            <div class="product-description">
                                                                <h5><a href="#">
                                                                        <? foreach ($phone_brands as $brand):
                                                                            if($brand['id']===$laptop['brandId']) echo "{$brand['title']} ";?>
                                                                        <? endforeach;?>
                                                                        <?=$laptop['model']?></a></h5>
                                                                <div class="price-box">
                                                                    <span class="price">$<?=number_format($laptop['price'],2)?></span>
                                                                </div>
                                                                <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            </div>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button">
                                                                        <a class="toch-button toch-add-cart add-to-cart" data-id="<?=$laptop['id']?>" href="/cart/add?id=<?=$laptop['id']?>" "><i class="fa fa-shopping-cart"></i> У кошик</a>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endforeach;?>
                                                <!-- End Single-Product -->
                                                <!-- Start Single-Product -->


                                                <!-- End Single-Product -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-2-2 -->

                                    <!-- End Product = display-2-3 -->
                                    <!-- Start Product = display-2-4 -->
                                    <div role="tabpanel" class="tab-pane fade" id="display-2-4">
                                        <div class="row">
                                            <div class="active-product-carosel">
                                                <!-- Start Single-Product -->
                                                <? foreach($asus as $laptop):
                                                    ?>
                                                    <div class="col-xs-12">
                                                        <div class="single-product">
                                                            <? if ($laptop['hit']==='1') echo
                                                            "<div class=\"label_new\">
                                                            <span class=\"new\">new</span>
                                                        </div>"
                                                            ?>
                                                            <div class="sale-off">
                                                                <span class="sale-percent">-55%</span>
                                                            </div>
                                                            <div class="product-img">
                                                                <a href="product/<?=$laptop['alias']?>">
                                                                    <img class="primary-img" src="/public/img/product/<?=$laptop['img']?>" alt="Product">
                                                                </a>
                                                            </div>
                                                            <div class="product-description">
                                                                <h5><a href="#">
                                                                        <? foreach ($phone_brands as $brand):
                                                                            if($brand['id']===$laptop['brandId']) echo "{$brand['title']} ";?>
                                                                        <? endforeach;?>
                                                                        <?=$laptop['model']?></a></h5>
                                                                <div class="price-box">
                                                                    <span class="price">$<?=number_format($laptop['price'],2)?></span>
                                                                </div>
                                                                <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            </div>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button">
                                                                        <a class="toch-button toch-add-cart add-to-cart" data-id="<?=$laptop['id']?>" href="/cart/add?id=<?=$laptop['id']?>" "><i class="fa fa-shopping-cart"></i> У кошик</a>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endforeach;?>
                                                <!-- End Single-Product -->
                                                <!-- Start Single-Product -->


                                                <!-- End Single-Product -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-2-4 -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Product -->
                </div>
                <!-- END PRODUCT-AREA (2) -->
                <!-- START PRODUCT-AREA (3) -->
                <div class="product-area">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <!-- Start Product-Menu -->
                            <div class="product-menu">
                                <div class="product-title">
                                    <h3 class="title-group-2 gfont-1">Smart Phones</h3>
                                </div>

                                <ul role="tablist">
                                    <li role="presentation"><a href="#display-3-2" role="tab" data-toggle="tab">Apple</a></li>
                                    <li role="presentation"><a href="#display-3-3"  role="tab" data-toggle="tab">Samsung</a></li>
                                    <li role="presentation"><a href="#display-3-4"  role="tab" data-toggle="tab">LG</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Product-Menu -->
                    <!-- Start Product -->
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="product carosel-navigation">
                                <div class="tab-content">
                                    <!-- Start Product = display-3-1 -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="display-3-2">
                                        <div class="row">
                                            <div class="active-product-carosel">
                                                <!-- Start Single-Product -->
                                                <? foreach($apple as $laptop):
                                                    ?>
                                                    <div class="col-xs-12">
                                                        <div class="single-product">
                                                            <? if ($laptop['hit']==='1') echo
                                                            "<div class=\"label_new\">
                                                            <span class=\"new\">new</span>
                                                        </div>"
                                                            ?>
                                                            <div class="sale-off">
                                                                <span class="sale-percent">-55%</span>
                                                            </div>
                                                            <div class="product-img">
                                                                <a href="product/<?=$laptop['alias']?>">
                                                                    <img class="primary-img" src="/public/img/product/<?=$laptop['img']?>" alt="Product">
                                                                </a>
                                                            </div>
                                                            <div class="product-description">
                                                                <h5><a href="#">
                                                                        <?=$laptop['model']?></a></h5>
                                                                <div class="price-box">
                                                                    <span class="price">$<?=number_format($laptop['price'],2)?></span>
                                                                </div>
                                                                <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            </div>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button">
                                                                        <a class="toch-button toch-add-cart add-to-cart" data-id="<?=$laptop['id']?>" href="/cart/add?id=<?=$laptop['id']?>" "><i class="fa fa-shopping-cart"></i> У кошик</a>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endforeach;?>
                                                <!-- End Single-Product -->
                                                <!-- Start Single-Product -->


                                                <!-- End Single-Product -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-3-1 -->
                                    <!-- Start Product = display-3-2 -->

                                    <!-- End Product = display-3-2 -->
                                    <!-- Start Product = display-3-3 -->
                                    <div role="tabpanel" class="tab-pane fade" id="display-3-3">
                                        <div class="row">
                                            <div class="active-product-carosel">
                                                <!-- Start Single-Product -->
                                                <? foreach($samsung as $laptop):
                                                    ?>
                                                    <div class="col-xs-12">
                                                        <div class="single-product">
                                                            <? if ($laptop['hit']==='1') echo
                                                            "<div class=\"label_new\">
                                                            <span class=\"new\">new</span>
                                                        </div>"
                                                            ?>
                                                            <div class="sale-off">
                                                                <span class="sale-percent">-55%</span>
                                                            </div>
                                                            <div class="product-img">
                                                                <a href="product/<?=$laptop['alias']?>">
                                                                    <img class="primary-img" src="/public/img/product/<?=$laptop['img']?>" alt="Product">
                                                                </a>
                                                            </div>
                                                            <div class="product-description">
                                                                <h5><a href="#">

                                                                        <?=$laptop['model']?></a></h5>
                                                                <div class="price-box">
                                                                    <span class="price">$<?=number_format($laptop['price'],2)?></span>
                                                                </div>
                                                                <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            </div>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button">
                                                                        <a class="toch-button toch-add-cart add-to-cart" data-id="<?=$laptop['id']?>" href="/cart/add?id=<?=$laptop['id']?>" "><i class="fa fa-shopping-cart"></i> У кошик</a>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endforeach;?>
                                                <!-- End Single-Product -->
                                                <!-- Start Single-Product -->


                                                <!-- End Single-Product -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-3-3 -->
                                    <!-- Start Product = display-3-4 -->
                                    <div role="tabpanel" class="tab-pane fade" id="display-3-4">
                                        <div class="row">
                                            <div class="active-product-carosel">
                                                <!-- Start Single-Product -->
                                                <? foreach($lg as $laptop):
                                                    ?>
                                                    <div class="col-xs-12">
                                                        <div class="single-product">
                                                            <? if ($laptop['hit']==='1') echo
                                                            "<div class=\"label_new\">
                                                            <span class=\"new\">new</span>
                                                        </div>"
                                                            ?>
                                                            <div class="sale-off">
                                                                <span class="sale-percent">-55%</span>
                                                            </div>
                                                            <div class="product-img">
                                                                <a href="product/<?=$laptop['alias']?>">
                                                                    <img class="primary-img" src="/public/img/product/<?=$laptop['img']?>" alt="Product">
                                                                </a>
                                                            </div>
                                                            <div class="product-description">
                                                                <h5><a href="#">
                                                                        <? foreach ($phone_brands as $brand):
                                                                            if($brand['id']===$laptop['brandId']) echo "{$brand['title']} ";?>
                                                                        <? endforeach;?>
                                                                        <?=$laptop['model']?></a></h5>
                                                                <div class="price-box">
                                                                    <span class="price">$<?=number_format($laptop['price'],2)?></span>
                                                                </div>
                                                                <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            </div>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button">
                                                                        <a class="toch-button toch-add-cart add-to-cart" data-id="<?=$laptop['id']?>" href="/cart/add?id=<?=$laptop['id']?>" "><i class="fa fa-shopping-cart"></i> У кошик</a>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endforeach;?>
                                                <!-- End Single-Product -->
                                                <!-- Start Single-Product -->


                                                <!-- End Single-Product -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-3-4 -->
                                </div>
                            </div>
                            <!-- End Product -->
                        </div>
                    </div>
                </div>
                <!-- END PRODUCT-AREA (3) -->
                <!-- START PRODUCT-BANNER -->
                <div class="product-banner">
                    <div class="row">
                        <div class="col-md-7 banner-box1">
                            <div class="single-product-banner">
                                <a target="_blank" href="https://www.samsung.com/ua/"><img src="/public/img/banner/Samsung_Logo.svg%20(1).png" alt="Product Banner"></a>

                            </div>
                        </div>
                        <div class="col-md-5 banner-box2">
                            <div class="single-product-banner">
                                <a target="_blank" href="https://www.lg.com/ua"><img src="/public/img/banner/lg.png" alt="Product Banner"></a>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PRODUCT-BANNER -->
                <!-- START  -->
                <!-- START SMALL-PRODUCT-AREA (1) -->
                <div class="small-product-area">
                    <!-- Start Product-Menu -->
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="product-menu">
                                <ul role="tablist">
                                    <li role="presentation" class=" active"><a href="#display-4-1" role="tab" data-toggle="tab">Новинки</a></li>
                                    <li role="presentation"><a href="#display-4-2" role="tab" data-toggle="tab">Розпродаж</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Product-Menu -->
                    <!-- Start Product -->
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="product carosel-navigation">
                                <div class="tab-content">
                                    <!-- Product = display-4-1 -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="display-4-1">
                                        <div class="row">
                                            <div class="active-small-product">
                                                <!-- Start Single-Product -->
                                                <?
                                                $j = 0;
                                                foreach ($count as $cont):
                                                   for ($k = 0;$k<ceil($cont['Count']/3);$k++):
                                                ?>
                                                <div class="col-xs-12">
                                                    <?
                                                    $tmp = $j*3;
                                                    $products = \R::getAll("SELECT * FROM Product WHERE yearOfManufacturing>2018 LIMIT 3 OFFSET {$tmp}");
                                                    $j++;
                                                    foreach ($products as $prod):?>
                                                        <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="product/<?=$prod['alias']?>">
                                                                <img class="primary-img small-img" src="/public/img/product/<?=$prod['img']?>" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="product/<?=$prod['alias']?>"><?=$prod['model']?></a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$<?=number_format($prod['price'],2)?></span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="product-button-2">
                                                                    <a style="padding: 0;" class="add-to-cart" data-id="<?=$prod['id']?>" href="/cart/add?id=<?=$prod['id']?>" data-toggle="tooltip" title="У кошик"><i class="fa fa-shopping-cart"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?endforeach;?>
                                                </div>
                                                <?endfor;
                                                   endforeach;?>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-4-1 -->
                                    <!-- Start Product = display-4-2 -->
                                    <div role="tabpanel" class="tab-pane fade" id="display-4-2">
                                        <div class="row">
                                            <div class="active-small-product">
                                                <!-- Start Single-Product -->
                                                <?
                                                $j = 0;
                                                foreach ($count as $cont):
                                                    for ($k = 0;$k<ceil($cont['Count']/3);$k++):
                                                        ?>
                                                        <div class="col-xs-12">
                                                            <?
                                                            $tmp = $j*3;
                                                            $products = \R::getAll("SELECT * FROM Product WHERE hit='1' LIMIT 3 OFFSET {$tmp}");
                                                            $j++;
                                                            foreach ($products as $prod):?>
                                                                <div class="single-product">
                                                                    <div class="product-img">
                                                                        <a href="product/<?=$prod['alias']?>">
                                                                            <img class="primary-img small-img" src="/public/img/product/<?=$prod['img']?>" alt="Product">
                                                                        </a>
                                                                    </div>
                                                                    <div class="product-description">
                                                                        <h5><a href="product/<?=$prod['alias']?>"><?=$prod['model']?></a></h5>
                                                                        <div class="price-box">
                                                                            <span class="price">$<?=number_format($prod['price'],2)?></span>
                                                                        </div>
                                                                        <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                                        <div class="product-action">
                                                                            <div class="product-button-2">
                                                                                <a style="padding: 0;" class="add-to-cart" data-id="<?=$prod['id']?>" href="/cart/add?id=<?=$prod['id']?>" data-toggle="tooltip" title="У кошик"><i class="fa fa-shopping-cart"></i></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?endforeach;?>
                                                        </div>
                                                    <?endfor;
                                                endforeach;?>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-4-2 -->
                                    <!-- Start Product = display-4-3 -->
                                    <div role="tabpanel" class="tab-pane fade" id="display-4-3">
                                        <div class="row">
                                            <div class="active-small-product">
                                                <!-- Start Single-Product -->
                                                <div class="col-xs-12">
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/1.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Trid Palm</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$99.00</span>
                                                                <span class="old-price">$120.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/5.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Established Fact</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$85.00</span>
                                                                <span class="old-price">$110.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/3.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Various Versions</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$90.00</span>
                                                                <span class="old-price">$120.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Single-Product -->
                                                <!-- Start Single-Product -->
                                                <div class="col-xs-12">
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/1.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Established Fact</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$85.00</span>
                                                                <span class="old-price">$105.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/5.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Trid Palm</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$99.00</span>
                                                                <span class="old-price">$120.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/3.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Established Fact</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$85.00</span>
                                                                <span class="old-price">$110.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Single-Product -->
                                                <!-- Start Single-Product -->
                                                <div class="col-xs-12">
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/1.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Various Versions</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$90.00</span>
                                                                <span class="old-price">$120.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/5.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Established Fact</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$85.00</span>
                                                                <span class="old-price">$105.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/3.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Trid Palm</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$99.00</span>
                                                                <span class="old-price">$120.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Single-Product -->
                                                <!-- Start Single-Product -->
                                                <div class="col-xs-12">
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/1.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Established Fact</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$85.00</span>
                                                                <span class="old-price">$110.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/5.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Various Versions</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$90.00</span>
                                                                <span class="old-price">$120.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/3.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Established Fact</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$85.00</span>
                                                                <span class="old-price">$105.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Single-Product -->
                                                <!-- Start Single-Product -->
                                                <div class="col-xs-12">
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/1.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Trid Palm</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$99.00</span>
                                                                <span class="old-price">$120.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/5.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Established Fact</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$85.00</span>
                                                                <span class="old-price">$110.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="/public/img/product/small/3.jpg" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#">Various Versions</a></h5>
                                                            <div class="price-box">
                                                                <span class="price">$90.00</span>
                                                                <span class="old-price">$120.00</span>
                                                            </div>
                                                            <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Single-Product -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-4-3 -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Product -->
                </div>
                <!-- END SMALL-PRODUCT-AREA (1) -->
            </div>
        </div>
    </div>
    <!-- START BRAND-LOGO-AREA -->
    <div class="brand-logo-area carosel-navigation">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="area-title">
                        <h3 class="title-group border-red gfont-1">Ми співпрацюємо з такими брендами:</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="active-brand-logo">
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="/public/img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="/public/img/brand/2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="/public/img/brand/3.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="/public/img/brand/4.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="/public/img/brand/5.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="/public/img/brand/6.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="/public/img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="/public/img/brand/2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="/public/img/brand/3.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END BRAND-LOGO-AREA -->
    <!-- START SUBSCRIBE-AREA -->
    <? foreach($phones as $phone):?>
    <div id="quickview-wrapper">
        <!-- Modal -->
        <div class="modal fade" id="productModal-1-<?=$phone['id'];?>" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-product">
                            <div class="product-images">
                                <div class="main-image images">
                                    <img alt="#" src="img/product/<?=$phone['img'];?>"/>
                                </div>
                            </div><!-- .product-images -->

                            <div class="product-info">
                                <h1><?=$phone['model']?></h1>
                                <div class="price-box-3">
                                    <hr />
                                    <div class="s-price-box">
                                        <span class="new-price">$<?=number_format($phone['price'],2);?></span>
                                    </div>
                                    <hr />
                                </div>
                                <a href="shop.html" class="see-all">See all features</a>
                                <div class="quick-add-to-cart">


<!--                                        <button class="single_add_to_cart_button type="submit">У кошик</button>-->

                                            <button id="add-to-cart" data-id="<?=$phone['id'];?>" onclick="window.location.href='/cart/add?id=<?=$phone['id']?>'"  class="single_add_to_cart_button">У кошик</button>

                                </div>
                                <div class="quick-desc">
                                    Найкращі телефони, планшети та ноутбуки лише у нашому магазині.Купляйте тільки у нас і ви завжди будете задоволені якістю товару!
                                </div>
                                <div class="social-sharing">
                                    <div class="widget widget_socialsharing_widget">
                                        <h3 class="widget-title-modal">Поділитись продуктом</h3>
                                        <ul class="social-icons">
                                            <li><a target="_blank" title="Facebook" href="#" class="facebook social-icon"><i class="fa fa-facebook"></i></a></li>
                                            <li><a target="_blank" title="Twitter" href="#" class="twitter social-icon"><i class="fa fa-twitter"></i></a></li>
                                            <li><a target="_blank" title="Pinterest" href="#" class="pinterest social-icon"><i class="fa fa-pinterest"></i></a></li>
                                            <li><a target="_blank" title="Google +" href="#" class="gplus social-icon"><i class="fa fa-google-plus"></i></a></li>
                                            <li><a target="_blank" title="LinkedIn" href="#" class="linkedin social-icon"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .product-info -->
                        </div><!-- .modal-product -->
                    </div><!-- .modal-body -->
                </div><!-- .modal-content -->
            </div><!-- .modal-dialog -->
        </div>
        <!-- END Modal -->
    </div>
    <? endforeach;?>
    <? foreach($tablets as $phone):?>
        <div id="quickview-wrapper">
            <!-- Modal -->
            <div class="modal fade" id="productModal-2-<?=$phone['id'];?>" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="modal-product">
                                <div class="product-images">
                                    <div class="main-image images">
                                        <img alt="#" src="img/product/<?=$phone['img'];?>"/>
                                    </div>
                                </div><!-- .product-images -->

                                <div class="product-info">
                                    <h1><?=$phone['model']?></h1>
                                    <div class="price-box-3">
                                        <hr />
                                        <div class="s-price-box">
                                            <span class="new-price">$<?=number_format($phone['price'],2);?></span>
                                        </div>
                                        <hr />
                                    </div>
                                    <a href="shop.html" class="see-all">See all features</a>
                                    <div class="quick-add-to-cart">


                                        <!--                                        <button class="single_add_to_cart_button type="submit">У кошик</button>-->

                                        <button id="add-to-cart" data-id="<?=$phone['id'];?>" onclick="window.location.href='/cart/add?id=<?=$phone['id']?>'"  class="single_add_to_cart_button">У кошик</button>

                                    </div>
                                    <div class="quick-desc">
                                        Найкращі телефони, планшети та ноутбуки лише у нашому магазині.Купляйте тільки у нас і ви завжди будете задоволені якістю товару!
                                    </div>
                                    <div class="social-sharing">
                                        <div class="widget widget_socialsharing_widget">
                                            <h3 class="widget-title-modal">Поділитись продуктом</h3>
                                            <ul class="social-icons">
                                                <li><a target="_blank" title="Facebook" href="#" class="facebook social-icon"><i class="fa fa-facebook"></i></a></li>
                                                <li><a target="_blank" title="Twitter" href="#" class="twitter social-icon"><i class="fa fa-twitter"></i></a></li>
                                                <li><a target="_blank" title="Pinterest" href="#" class="pinterest social-icon"><i class="fa fa-pinterest"></i></a></li>
                                                <li><a target="_blank" title="Google +" href="#" class="gplus social-icon"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a target="_blank" title="LinkedIn" href="#" class="linkedin social-icon"><i class="fa fa-linkedin"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div><!-- .product-info -->
                            </div><!-- .modal-product -->
                        </div><!-- .modal-body -->
                    </div><!-- .modal-content -->
                </div><!-- .modal-dialog -->
            </div>
            <!-- END Modal -->
        </div>
    <? endforeach;?>
    <? foreach($laptops as $phone):?>
        <div id="quickview-wrapper">
            <!-- Modal -->
            <div class="modal fade" id="productModal-3-<?=$phone['id'];?>" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="modal-product">
                                <div class="product-images">
                                    <div class="main-image images">
                                        <img alt="#" src="img/product/<?=$phone['img'];?>"/>
                                    </div>
                                </div><!-- .product-images -->

                                <div class="product-info">
                                    <h1><?=$phone['model']?></h1>
                                    <div class="price-box-3">
                                        <hr />
                                        <div class="s-price-box">
                                            <span class="new-price">$<?=number_format($phone['price'],2);?></span>
                                        </div>
                                        <hr />
                                    </div>
                                    <a href="shop.html" class="see-all">See all features</a>
                                    <div class="quick-add-to-cart">


                                        <!--                                        <button class="single_add_to_cart_button type="submit">У кошик</button>-->

                                        <button id="add-to-cart" data-id="<?=$phone['id'];?>" onclick="window.location.href='/cart/add?id=<?=$phone['id']?>'"  class="single_add_to_cart_button">У кошик</button>

                                    </div>
                                    <div class="quick-desc">
                                        Найкращі телефони, планшети та ноутбуки лише у нашому магазині.Купляйте тільки у нас і ви завжди будете задоволені якістю товару!
                                    </div>
                                    <div class="social-sharing">
                                        <div class="widget widget_socialsharing_widget">
                                            <h3 class="widget-title-modal">Поділитись продуктом</h3>
                                            <ul class="social-icons">
                                                <li><a target="_blank" title="Facebook" href="#" class="facebook social-icon"><i class="fa fa-facebook"></i></a></li>
                                                <li><a target="_blank" title="Twitter" href="#" class="twitter social-icon"><i class="fa fa-twitter"></i></a></li>
                                                <li><a target="_blank" title="Pinterest" href="#" class="pinterest social-icon"><i class="fa fa-pinterest"></i></a></li>
                                                <li><a target="_blank" title="Google +" href="#" class="gplus social-icon"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a target="_blank" title="LinkedIn" href="#" class="linkedin social-icon"><i class="fa fa-linkedin"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div><!-- .product-info -->
                            </div><!-- .modal-product -->
                        </div><!-- .modal-body -->
                    </div><!-- .modal-content -->
                </div><!-- .modal-dialog -->
            </div>
            <!-- END Modal -->
        </div>
    <? endforeach;?>
<!-- END QUICKVIEW PRODUCT -->
</section>

<!-- jquery
		============================================ -->

</body>
</html>



<?foreach ($products as $product):?>
    <!-- Start Single-Product -->
    <div class="single-product">
        <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="label_new">
                <span class="new">new</span>
            </div>
            <div class="sale-off">
                <span class="sale-percent">-55%</span>
            </div>
            <div class="product-img">
                <a href="/product/<?=$product['alias']?>">
                    <img class="primary-img" src="/public/img/product/<?=$product['img']?>" alt="Product">
                </a>
            </div>
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12">
            <div class="product-description">
                <h5><a href="/product/<?=$product['alias']?>"><?=$product['model']?></a></h5>
                <div class="price-box">
                    <span class="price">$<?=number_format($product['price'],2);?></span>
                </div>
                <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                <p style="font-size:15px;">
                    <b>Категорія: </b>
                    <?
                    if( $product['categoryId']===3){
                        echo 'Телефони';
                    }
                    elseif ($product['categoryId']===4){
                        echo 'Планшети';
                    }
                    else{
                        echo 'Ноутбуки';
                    }
                    ?></p>
                <b>Опис:</b>
                <p class="description">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Viva..</p>
                <div class="product-action">
                    <div class="button-group">
                        <div class="product-button">
                            <a class="toch-button toch-add-cart add-to-cart" data-id="<?=$phone['id']?>" href="/cart/add?id=<?=$phone['id']?>" "><i class="fa fa-shopping-cart"></i> У кошик</a>
                        </div>
                        <div class="product-button-2">
                            <a href="/product/<?=$product['alias']?>" class="modal-view" title="Quickview"><i class="fa fa-search-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Single-Product -->
    <!-- Start Single-Product -->
<?endforeach;?>
<div class="col-md-9">
    <!-- START PRODUCT-BANNER -->
    <div class="product-banner">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner">
                    <a href="#"><img src="/public/img/banner/12.jpg" alt="Product Banner"></a>
                </div>
            </div>
        </div>
    </div>
    <!-- END PRODUCT-BANNER -->
    <!-- START PRODUCT-AREA -->
    <div class="product-area">
        <div class="row">
            <div class="col-xs-12">
                <!-- Start Product-Menu -->
                <div class="product-menu">
                    <div class="product-title">
                        <h3 class="title-group-3 gfont-1">Товари в категорії - <b>
                                <?if($category) echo $category->name;
                                  else echo "Усі товари";
                                ?></b></h3>
                    </div>
                </div>
                <div class="product-filter">
                    <ul role="tablist">
                        <li role="presentation" class="list active">
                            <a href="#display-1-1" role="tab" data-toggle="tab"></a>
                        </li>
                        <li role="presentation"  class="grid ">
                            <a href="#display-1-2" role="tab" data-toggle="tab"></a>
                        </li>
                    </ul>
                    <div class="sort">
                        <label>Sort By:</label>
                        <select id="sort">
                            <option value="#">За замовчуванням</option>
                            <option value="#">Модель (Z - A))</option>
                            <option value="#">Модель (A - Z)</option>
                            <option value="#">Ціна (Вища > Нижча)</option>
                            <option value="#">Ціна (Нижча > Вища)</option>
                        </select>
                        <?if(isset($_SESSION['sort'])){
                        echo "<script type='text/javascript'>document.getElementById('sort').selectedIndex = {$_SESSION['sort']}</script>";
                        }
                        ?>
                    </div>

                </div>

                <!-- End Product-Menu -->
                <div class="clear"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <!-- Start Product -->
                <div class="product">
                    <div class="tab-content">
                        <!-- Product -->
                        <div role="tabpanel" class="tab-pane fade in active" id="display-1-1">
                            <?if(!empty($products)):?>
                            <div class="row" id="#goods">
                                <div class="listview">
                                    <?foreach ($products as $product):?>

                                    <!-- Start Single-Product -->
                                    <div class="single-product">
                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                            <div class="label_new">
                                                <span class="new">new</span>
                                            </div>
                                            <div class="sale-off">
                                                <span class="sale-percent">-55%</span>
                                            </div>
                                            <div class="product-img">
                                                <a href="/product/<?=$product['alias']?>">
                                                    <img class="primary-img" src="/public/img/product/<?=$product['img']?>" alt="Product">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-8 col-xs-12">
                                            <div class="product-description">
                                                <h5><a href="/product/<?=$product['alias']?>"><?=$product['model']?></a></h5>
                                                <div class="price-box">
                                                    <span class="price">$<?=number_format($product['price'],2);?></span>
                                                </div>
                                                <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
                                                <p style="font-size:15px;">
                                                    <b>Категорія: </b>
                                                    <?
                                                    if( $product['categoryId']==3){
                                                        echo 'Телефони';
                                                    }
                                                    elseif ($product['categoryId']==4){
                                                        echo 'Планшети';
                                                    }
                                                    else{
                                                        echo 'Ноутбуки';
                                                    }
                                                   ?></p>
                                                <b>Опис:</b>
                                                <p class="description">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Viva..</p>
                                                <div class="product-action">
                                                    <div class="button-group">
                                                        <div class="product-button">
                                                            <a class="toch-button toch-add-cart add-to-cart" data-id="<?=$product['id']?>" href="/cart/add?id=<?=$product['id']?>" "><i class="fa fa-shopping-cart"></i> У кошик</a>
                                                        </div>
                                                        <div class="product-button-2">
                                                            <a href="/product/<?=$product['alias']?>" class="modal-view" title="Quickview"><i class="fa fa-search-plus"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Single-Product -->
                                    <!-- Start Single-Product -->
                                    <?endforeach;?>
                                </div>
                            </div>
                            <!-- Start Pagination Area -->
                            <div class="pagination-area">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <?=($pagination)?>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="product-result">
                                            <span>Показано товари з <?=$start+1?> по
                                                <?
                                                if($start+3<$total) {
                                                   echo $start + $perpage;
                                                }
                                                else{
                                                    if($total!==0) {
                                                        echo ($start + $perpage) - (($start + $perpage) % $total);
                                                    }
                                                }
                                                ?> з <?=$total?> загалом (<?=ceil($total/$perpage)?> сторінки)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Pagination Area -->
                            <?else:?>
                            <p class="text-muted" style="font-size: 25px;">Товарів не знайдено</p>
                            <?endif;?>
                        </div>
                        <!-- End Product -->
                        <!-- Start Product-->

                        <!-- End Product = TV -->
                    </div>
                </div>
                <!-- End Product -->
            </div>
        </div>
    </div>
    <!-- END PRODUCT-AREA -->
</div>

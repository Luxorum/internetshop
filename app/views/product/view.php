
<div class="col-md-12 col-sm-12 col-xs-12">
    <!-- Start Toch-Prond-Area -->
    <div class="toch-prond-area">
        <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-12">
                <div class="clear"></div>
                <div class="tab-content">
                    <a id="base-price" style="display: none;" data-price="<?=$product->price?>"></a>
                    <!-- Product = display-1-1 -->
                    <div role="tabpanel" class="tab-pane fade in active" id="display-1">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="toch-photo">
                                    <a href="#"><img src="/public/img/toch/1.jpg" data-imagezoom="true" alt="#" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Product = display-1-1 -->
                    <!-- Start Product = display-1-2 -->
                    <div role="tabpanel" class="tab-pane fade" id="display-2">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="toch-photo">
                                    <a href="#"><img src="/public/img/toch/2.jpg" data-imagezoom="true" alt="#" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Product = display-1-2 -->
                    <!-- Start Product = di3play-1-3 -->
                    <div role="tabpanel" class="tab-pane fade" id="display-3">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="toch-photo">
                                    <a href="#"><img src="/public/img/toch/3.jpg" data-imagezoom="true" alt="#" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Product = display-1-3 -->
                    <!-- Start Product = di3play-1-4 -->
                    <div role="tabpanel" class="tab-pane fade" id="display-4">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="toch-photo">
                                    <a href="#"><img src="/public/img/toch/4.jpg" data-imagezoom="true" alt="#" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Product = display-1-4 -->
                </div>
                <!-- Start Toch-prond-Menu -->
                <div class="toch-prond-menu">
                    <ul role="tablist">
                        <li role="presentation" class=" active"><a href="#display-1" role="tab" data-toggle="tab"><img src="/public/img/toch/1.jpg" alt="#" /></a></li>
                        <li role="presentation"><a href="#display-2" role="tab" data-toggle="tab"><img src="/public/img/toch/2.jpg" alt="#" /></a></li>
                        <li role="presentation"><a href="#display-3"  role="tab" data-toggle="tab"><img src="/public/img/toch/3.jpg" alt="#" /></a></li>
                        <li role="presentation"><a href="#display-4"  role="tab" data-toggle="tab"><img src="/public/img/toch/4.jpg" alt="#" /></a></li>
                    </ul>
                </div>
                <!-- End Toch-prond-Menu -->
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
                <h2 class="title-product product-title"> <?=$product->model;?></h2>
                <div class="about-toch-prond">
                    <p>
											<span class="rating">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-o"></i>
											</span>
                        <a href="#">1 reviews</a>
                        /
                        <a href="#">Write a review</a>
                    </p>
                    <hr />
                    <p class="short-description">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nul... </p>
                    <hr />
                    <span class="current-price product-price">$<?=number_format($product->price,2)?></span>
                    <span class="item-stock">Наявність: <span class="text-stock">На складі</span></span>
                </div>
                <div class="about-product">
                    <div class="product-select product-color">
                        <label><sup>*</sup>Колір</label>
                        <select id="color" class="form-control">
                            <? foreach ($colors as $color): ?>
                                <option data-price="<?=$color['price'];?>"><?=$color['name'];
                                    if($color['price']!=0) echo " (+$" . number_format($color['price'],2) . ")";?></option>
                            <?endforeach;?>
                        </select>
                    </div>
                    <div class="product-select product-Size">
                        <label><sup>*</sup>Пакет послуг</label>
                        <select id="size" class="form-control">
                            <? foreach ($sizes as $size): ?>
                            <option data-price="<?=$size['price'];?>"><?=$size['name'];
                                    if($size['price']!=0) echo " (+$" . number_format($size['price'],2) , ")";?></option>
                            <?endforeach;?>
                        </select>
                    </div>




                </div>
                <a class="product-quantity">
                    <span>Кількість</span>
                    <input class="quantity" type="number" min="1" max="10" placeholder="1" />
                    <a class="toch-button toch-add-cart add-to-cart" data-id="<?=$product->id?>" href="/cart/add?id=<?=$product->id?>" ">У кошик</a>

                    <hr />
                    <p class="text-muted">Ми дуже цінуємо наших клієнтів та будемо вам вдячні якщо ви підпишетесь на нас по пошті або в соціальних мережах</p>
                </div>
            </div>
        </div>
        <!-- Start Toch-Box -->
        <div class="toch-box">
            <div class="row">
                <div class="col-xs-12">
                    <!-- Start Toch-Menu -->
                    <div class="toch-menu">
                        <ul role="tablist">
                            <li role="presentation" class=" active"><a href="#description" role="tab" data-toggle="tab">Опис товару</a></li>
                                                    </ul>
                    </div>
                    <!-- End Toch-Menu -->
                    <div class="tab-content toch-description-review">
                        <!-- Start display-description -->
                        <div role="tabpanel" class="tab-pane fade in active" id="description">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="toch-description">
                                        <p>Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt.
                                            Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt.
                                            Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt.
                                            Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt.
                                            Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt.
                                            Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End display-description -->
                        <!-- Start display-reviews -->
                        <div role="tabpanel" class="tab-pane fade" id="reviews">
                            <div class="row">
                                <div class="col-xs-12">
                                </div>
                            </div>
                        </div>
                        <!-- End Product = display-reviews -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Toch-Box -->


<?echo "Пошук за назвою товару: <b>{$query}</b>";?>
<? if(!empty($products)): ?>
<? foreach($products as $product):
    ?>
    <div style="margin-bottom: 10px;" class="col-md-3">
        <div class="single-product">
            <? if ($product['hit']==='1') echo
            "<div class=\"label_new\">
                                                            <span class=\"new\">new</span>
                                                        </div>"
            ?>
            <div class="sale-off">
                <span class="sale-percent">-55%</span>
            </div>
            <div class="product-img">
                <a href="product/<?=$product['alias']?>">
                    <img class="primary-img" src="/public/img/product/<?=$product['img']?>" alt="Product">
                </a>
            </div>
            <div class="product-description">
                <h5><a href="#"><?=$product['model']?></a></h5>
                <div class="price-box">
                    <span class="price">$<?=number_format($product['price'],2)?></span>
                </div>
                <span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
            </div>
            <div class="product-action">
                <div class="button-group">
                    <div class="product-button">
                        <a class="toch-button toch-add-cart add-to-cart" data-id="<?=$product['id']?>" href="/cart/add?id=<?=$laptop['id']?>" "><i class="fa fa-shopping-cart"></i> У кошик</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endforeach;?>
<?endif;?>
<?if(empty($products)){
  echo "<p style='font-size:40px;' class='text-muted'>Товарів не знайдено</p>";
}?>


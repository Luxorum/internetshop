<?php


namespace app\widgets\filter;




use internetshop\DB;

class Filter
{
    public $groups;
    public $attrs;
    public $tpl;

    public function __construct(){
        DB::instance();
        $this->tpl = __DIR__ . '/filter_tpl.php';
        $this->run();
    }

    protected function run(){
        if(!$this->groups){
            $this->groups = $this->getGroups();
        }
        if(!$this->attrs){
            $this->attrs = $this->getAttrs();
        }

        $filters = $this->getHtml();
        echo $filters;
    }

    protected function getHtml(){
        ob_start();
        $filter = self::getFilter();
        if(!empty($filter)){
            $filter = explode(',', $filter);
        }
        require $this->tpl;
        return ob_get_clean();
    }



    protected function getGroups(){
        return \R::getAssoc('SELECT id, title FROM Attribute_Group');
    }

    protected static function getAttrs(){
        $data = \R::getAssoc('SELECT * FROM Attribute_Value');
        $attrs = [];
        foreach($data as $k => $v){
            $attrs[$v['attribute_group_id']][$k] = $v['value'];
        }
        return $attrs;
    }
    public static function getFilter(){
        $filter = null;
        if(!empty($_GET['filter'])){
            $filter = preg_replace("#[^\d,]+#", '', $_GET['filter']);
            $filter = trim($filter, ',');
        }
        return $filter;
    }
    public static function getCountGroups($filter){
        $filters = explode(',', $filter);
        $attrs = self::getAttrs();
        $data = [];
        foreach($attrs as $key => $item){
            foreach($item as $k => $v){
                if(in_array($k, $filters)){
                    $data[] = $key;
                    break;
                }
            }
        }
        return count($data);
    }
}
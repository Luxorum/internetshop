<div class="accordion_one">
    <?php foreach($this->groups as $group_id => $group_item): ?>
    <h4><a class="accordion-trigger" data-toggle="collapse" href="#div<?=$group_id;?>"><?=$group_item;?></a></h4>
    <div id="div<?=$group_id?>" class="collapse in">
        <div class="filter-menu">
            <ul>
                <?if(!empty($this->attrs[$group_id])):?>
                <?php foreach($this->attrs[$group_id] as $attr_id => $value): ?>
                    <li><input type="checkbox" name="<?=$value?>" value="<?=$attr_id;?>"> <?=$value;?></li>
                <?endforeach;?>
                <?else:?>
                    <p class="text-muted">У цій групі немає фільтрів</p>
                <?endif;?>

            </ul>
        </div>
    </div>
    <?endforeach;?>
</div>
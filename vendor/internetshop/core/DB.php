<?php


namespace internetshop;


class DB
{
    use TSingletone;

    protected function __construct()
    {
        $db = require_once CONF . '/DB_config.php';
        class_alias('\RedBeanPHP\R','\R');
        \R::setup($db['dsn'],$db['user'],$db['pass']);
        if(!\R::testConnection()){
            throw new \Exception('Дб не підключено', 500);
        }
        \R::freeze(true);
        if(DEBUG){
            \R::debug(true, 1);
        }
    }
}
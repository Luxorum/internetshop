<?php


namespace internetshop\base;


use internetshop\DB;

abstract class Model
{
    public $attributes = [];
    public $errirs = [];
    public $rules = [];

    public function __construct()
    {
        DB::instance();
    }
    public function load($data){
        foreach($this->attributes as $name => $value){
            if(isset($data[$name])){
                $this->attributes[$name] = $data[$name];
            }
        }
    }
    public function save($table, $valid = true){
        if($valid){
            $tbl = \R::dispense($table);
        }
        foreach($this->attributes as $name => $value){
            $tbl->$name = $value;
        }
        return \R::store($tbl);
    }
}
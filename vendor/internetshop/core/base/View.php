<?php


namespace internetshop\base;


use mysql_xdevapi\Exception;

class View
{
    public $route;
    public $controller;
    public $model;
    public $view;
    public $prefix;
    public $data = [];
    public $meta = [];
    public $layout;

    public function __construct($route, $layout = '', $view = '', $meta)
    {
        $this->route = $route;
        $this->controller = $route['controller'];
        $this->view = $view;
        $this->model = $route['controller'];
        $this->prefix = $route['prefix'];
        $this->meta = $meta;
        if ($layout === false) {
            $this->layout = false;
        } else {
            $this->layout = $layout ?: LAYOUT;
        }
    }

    public function render($data)
    {
        if (is_array($data)) extract($data);
        $viewfile = APP . "/views/{$this->prefix}{$this->controller}/{$this->view}.php";
        if (is_file($viewfile)) {
            ob_start();
            require_once $viewfile;
            $content = ob_get_clean();
        } else {
            throw new \Exception("Не знайдено вид $viewfile", 500);
        }
        if ($this->layout !== false) {
            $layoutfile = APP . "/views/layouts/{$this->layout}.php";
            if (is_file($layoutfile)) {
                require_once $layoutfile;
            } else {
                throw new \Exception("Не знайдено шаблон $this->layout", 500);
            }
        }
    }

    public function getMeta()
    {
        $output = '<title>' . $this->meta['title'] . '</title>' . PHP_EOL;
        $output .= '<meta name="description" content="' . $this->meta['desc'] . '">' . PHP_EOL;
        $output .= '<meta name="keywords" content="' . $this->meta['keywords'] . '">' . PHP_EOL;
        return $output;
    }
}